<?php $title = "高校作业系统学生端"?>

<!doctype html>
<html lang="zh-CN">

<head>
    <!-- 必须的 meta 标签 -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap 的 CSS 文件 -->
    <link rel="stylesheet" href="../../assets/framework/bootstrap-4.6.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/my/css/main.css">

    <title><?php echo $title ?></title>
</head>

<body>

