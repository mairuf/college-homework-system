<!-- JavaScript 文件是可选的。从以下两种建议中选择一个即可！ -->
<!-- 选项 1：jQuery 和 Bootstrap 集成包（集成了 Popper） -->

<div class="container-fluid">
    <blockquote class="blockquote text-center">
        <p class="mb-0">爱上一个地方，就应该背上包去旅行，走得更远。</p>
        <footer class="blockquote-footer">出自商务印书馆的 <cite title="Source Title">《新华字典》</cite></footer>
    </blockquote>
</div>


<script src="/assets/framework/js/jquery-3.6.0.js">
</script>
<script src="/assets/framework/bootstrap-4.6.1/js/bootstrap.bundle.js">
</script>
</body>

</html>