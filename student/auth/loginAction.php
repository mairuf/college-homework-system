<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../sql/connection.php';

$username = $_POST['username'];
$pwd = $_POST['pwd'];

$querySql = "select id, create_time, update_time, stu_ID, stu_name, stu_pwd, class_ID, is_auth, is_delete from student_info where stu_ID='$username' and is_delete = 0";

// 查询语句
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);
$stuInfo = mysqli_fetch_array($result);
//echo $result_config;
if (mysqli_num_rows($result) <= 0) {
	echo "
<script>
    alert('学号未注册或已被禁用！');
    history.go(-1);
</script>";
} else {
	
	if ($stuInfo['is_auth'] == 0) {
		echo "
            <script>
                alert('该学号还未通过验证！');
                history.back();
            </script>
        ";
	} else {
		$now = date('Y-m-d H:i:s', time());
		if (password_verify($pwd, $stuInfo['stu_pwd'])) {
			setcookie("studentName", $stuInfo['stu_name'], path: '/');
			setcookie("studentID", $stuInfo['id'], path: '/');
			setcookie("studentNum", $stuInfo['stu_ID'], path: '/');
			setcookie("classID", $stuInfo['class_ID'], path: '/');
			setcookie("role", '学生', path: '/');
			
			echo "
            <script>
                alert('登录成功！');
                window.location.href='/student/#';
            </script>
        ";
		} else {
			echo "
            <script>
                alert('登录失败，用户名或密码错误！');
                history.back();
            </script>
        ";
		}
	}
	
	
}

