<?php
if ($_COOKIE['adminName'] || $_COOKIE['adminID'] ||  $_COOKIE['role'] ){
	// 删除cookie，使用户登录状态失效

	setcookie("studentName",$_COOKIE['adminName'],time()-1, path: '/');
	setcookie("studentID",$_COOKIE['adminID'],time()-1, path: '/');
	setcookie("role", $_COOKIE['role'],time()-1, path: '/');
	echo "
            <script>
                alert('退出登录成功！');
                window.location.href='/student/#';
            </script>
        ";
}else{
	echo "
            <script>
                alert('退出登录失败！');
                history.back();
            </script>
        ";
}