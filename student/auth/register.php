<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<!-- Bootstrap 的 CSS 文件 -->
	<link rel="stylesheet" href="/assets/framework/bootstrap-4.6.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/my/css/main.css">
	<title>登录</title>
</head>
<body>

<!-- 主体部分 -->
<div class="container h-100 ">
	<div class="row justify-content-center h-100">
		<div class="card-body row align-content-center justify-content-center">
			<form action='registerAction.php' method='post' name="registerForm" id="registerForm" enctype="multipart/form-data">
				<div class='form-group row'>
					<label for='stuName' class='col-3 col-form-label'>姓&nbsp;&nbsp;&nbsp;&nbsp;名</label>
					<div class='col-sm-8'>
						<input type='text' id='stuName' name='stuName'
						       class='form-control mx-sm-3'
						       aria-describedby='nameHelpInline' required='required'
						>
					</div>
				</div>
				<br>
				<div class='form-group row'>
					<label for='stuNum' class='col-3 col-form-label'>学&nbsp;&nbsp;&nbsp;&nbsp;号</label>
					<div class='col-sm-8'>
						<input type='text' id='stuNum' name='stuNum'
						       class='form-control mx-sm-3'
						       aria-describedby='nameHelpInline' required='required'
						>
					</div>
				</div>
				<br>
				<div class='form-group row'>
					<label for='pwd' class='col-3 col-form-label'>密&nbsp;&nbsp;&nbsp;&nbsp;码</label>
					<div class='col-sm-8'>
						<input type='password' id='pwd' name='pwd'
						       class='form-control mx-sm-3'
						       aria-describedby='nameHelpInline' required='required'
						>
					</div>
				</div>
				<br>
				<span>班级格式：年级 + 专业全称 + 班级（如：<b>2022级计算机科学与技术1班</b>）</span>
				<div class='form-group row'>
					<label for='className' class='col-3 col-form-label'>班&nbsp;&nbsp;&nbsp;&nbsp;级</label>
					<div class='col-sm-8'>
						<input type='text' id='className' name='className'
						       class='form-control mx-sm-3'
						       aria-describedby='nameHelpInline' required='required'
						>
					</div>
				</div>
				<div class='row justify-content-end'>
					<button type='submit' class='btn btn-primary btn-lg btn-block'>注&nbsp;&nbsp;&nbsp;&nbsp;册</button>
					<a href="login.php" class='btn btn-info btn-lg btn-block'>去&nbsp;&nbsp;登&nbsp;&nbsp;录</a>
				</div>
			</form>

		</div>
	</div>


</div>


</body>


<script src="/assets/framework/js/jquery-3.6.0.js"></script>
<script src="/assets/framework/bootstrap-4.6.1/js/bootstrap.min.js"></script>
</html>
