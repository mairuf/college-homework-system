<?php require_once './base/header.php'; ?>
<?php require_once './auth/loginFilter.php'; ?>

<div class="container-fluid " style="border-bottom: 3px solid rgba(45,101,70,0.56); margin-bottom: 5px;">
	<nav class="navbar navbar-expand-lg navbar-light">
		<button class="navbar-toggler"
		        type="button"
		        data-toggle="collapse"
		        data-target="#navbarSupportedContent"
		        aria-controls="navbarSupportedContent"
		        aria-expanded="false"
		        aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="course/index.php" target="studentFrame">我的课程</a>
				</li>
			</ul>
			<!--<div class="dropdown show dropleft">
				<a class="dropdown-toggle " href="#" role="button" id="personalCenter" data-toggle="dropdown"
				   aria-haspopup="true" aria-expanded="false">
					张三
				</a>
				<div class="dropdown-menu" aria-labelledby="personalCenter">
					<a class="dropdown-item" href="#">退出登录</a>
				</div>
			</div>-->
			<?php
			if ($_COOKIE['studentName'] == null || $_COOKIE['studentID'] == null || $_COOKIE['role']== null ){
				?>
				<div class="dropdown show dropleft">

					<a class="text-dark" href="auth/login.php">登录</a>
				</div>
				<?php
			}else{
				?>
				<div class="dropdown show dropleft">
					<a class="dropdown-toggle text-dark" href="#" role="button" id="personalCenter" data-toggle="dropdown"
					   aria-haspopup="true" aria-expanded="false">
						<?=$_COOKIE['studentName']?>
					</a>
					<div class="dropdown-menu" aria-labelledby="personalCenter">
						<a class="dropdown-item" href="auth/logout.php">退出登录</a>
					</div>
				</div>
				<?php
			}
			?>
		</div>
	
	</nav>
</div>
<!-- 左侧菜单栏 -->
<div class="container-fluid h-100">
	<div class="row h-100">

		<div class="col-12 h-100">
			<iframe src="course/index.php" class="w-100 h-100" name="studentFrame" style="border: 0;"></iframe>
		</div>
	</div>
</div>

<?php require_once './base/footer.php' ?>
