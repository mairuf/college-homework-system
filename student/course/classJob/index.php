<?php

// ini_set("display_errors", "On");
// error_reporting(E_ALL);

require_once '../../base/header-iframe.php';

require_once '../../../sql/connection.php';
$courseID = $_GET['courseID'];
?>

	<!-- 主体部分 -->
	<div class="container-fluid h-100">
		<!-- 导航部分信息 -->
		<ul class='nav justify-content-center'>
			<li class='nav-item'>
				<a class='nav-link active' href=''>我的作业列表（未完成）</a>
			</li>
			<li>
				<form class="form-inline" action="index.php" method="get" id="searchFrom">
					<input class="form-control mr-sm-2" type="search"
					       placeholder="搜索课程" aria-label="Search"
					       id="searchInfo" name="searchInfo"
					>
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
				</form>
			</li>
			<li class='nav-item'>
				<div class="btn-group " style="padding: 5px">
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
					        aria-haspopup="true" aria-expanded="false">
						未完成
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="index1.php?courseID=<?= $courseID ?>">已完成</a>
						<a class="dropdown-item" href="index.php?courseID=<?= $courseID ?>">未完成</a>
					</div>
				</div>
			</li>
		</ul>
		<!-- 列表 -->
		<table class='table table-striped text-center'>
			<thead>
			<tr>
				<!--        <th scope='col'>id</th>-->
				<th scope='col'>布置时间</th>
				<th scope='col'>最后一次更新时间</th>
				<th scope='col'>作业名称</th>
				<th scope='col'>作业类型</th>
				<th scope='col'>题目数量</th>
				<th scope='col'>操作</th>
			</tr>
			</thead>
			<tbody>

			<?php
			// 搜索信息，如果没有传入搜索信息，则设为 null
			$searchInfo = $_GET['searchInfo'] ?: null;
			// 页码
			$pageNum = intval($_GET['page'] ?: 1);
			// 数据库索引 ——
			$index = ($pageNum - 1) * 10;
			// $classID = $_COOKIE['classID'];
			$stuID = $_COOKIE['studentID'];
			// 无搜索信息
			if ($searchInfo == null) {
				// 统计学生所在班级的课程总数
				$countSql = "select count(*) as count from job_info where is_delete = 0
                                and course_ID = '$courseID'
                                and id NOT IN (select job_ID from `student-job` where stu_ID = '$stuID');";
				$selectSql = "select id, create_time, update_time, course_ID, job_name, job_type, question_total, job_total_points, template_file, is_delete from job_info
								where is_delete = 0
							  	and course_ID = '$courseID'
								and id NOT IN (select job_ID from `student-job` where stu_ID = '$stuID')
								limit $index, 10;";
			} // 有搜索信息
			else {
				$countSql = "select count(*) as count from job_info where is_delete = 0
                                and course_ID = '$courseID'
                                and id NOT IN (select job_ID from `student-job` where stu_ID = '$stuID')
								and  job_info.job_name like '%$searchInfo%';";
				$selectSql = "select id, create_time, update_time, course_ID, job_name, job_type, question_total, job_total_points, template_file, is_delete from job_info
								where is_delete = 0
								and course_ID = '$courseID'
							  	and id NOT IN (select job_ID from `student-job` where stu_ID = '$stuID')
                                and job_name like '%$searchInfo%' limit $index, 10;";
			}
			// 连接数据库，并查询
			$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
			// 获取管理员总数
			$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
			// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
			$page_sum = intval(ceil($count['count'] / 10));

			// 循环输出所有查询结果
			while ($row = mysqli_fetch_array($resultConfig)) {
				?>


				<tr>
					<td><?= $row['create_time'] ?></td>
					<td><?= $row['update_time'] ?></td>
					<td><?= $row['job_name'] ?></td>
					<td><?= $row['job_type'] == 1 ? "问卷形式" : "自定义形式" ?></td>
					<td><?= $row['question_total'] ?></td>
					<td>
						<?php if ($row['job_type'] == 1) { ?>
							<a class='btn btn-outline-primary'
							   href="../../answerQuestions/index.php?jobID=<?= $row['id'] ?>&jobName=<?= $row['job_name'] ?>">答题</a>
						<?php } else { ?>
							<?php if ($row['template_file']) { ?>
								<a class='btn btn-outline-primary' href="../../..<?= $row['template_file'] ?>">下载模板</a>
							<?php } else { ?>
								<button class='btn btn-outline-primary' disabled>下载模板（老师未上传）</button>
							<?php } ?>
							<a class='btn btn-outline-primary' href='#' data-toggle='modal' data-target='#upload'
							   onclick='uploadFile(<?= $row['id'] ?>,"<?= $row['job_name'] ?>")'>上传结果</a>
						<?php } ?>
					</td>
				</tr>

				<?php
			}

			?>
			</tbody>
		</table>
	</div>

	<!-- 文件上传部件 -->
	<div class='modal fade bd-example-modal-lg' id='upload' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>上传模板</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='uploadAction.php' method='post' target='studentFrame' name="upForm" id="upForm"
				      enctype="multipart/form-data">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='upCourseID' class='col-3 col-form-label'>课程ID</label>
							<div class='col-sm-8'>
								<input type='text' id='upCourseID' name='upCourseID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required' value="<?= $courseID ?>"
								>
							</div>
						</div>
						<div class='form-group row hiddenElement'>
							<label for='upJobID' class='col-3 col-form-label'>作业ID</label>
							<div class='col-sm-8'>
								<input type='text' id='upJobID' name='upJobID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<div class='form-group row hiddenElement'>
							<label for='upJobName' class='col-3 col-form-label'>作业名称</label>
							<div class='col-sm-8'>
								<input type='text' id='upJobName' name='upJobName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>

						<div class='form-group row'>
							<label for='templateFile' class='col-3 col-form-label'>文件</label>
							<div class='col-sm-8'>
								<input type='file' id='templateFile' name='templateFile'
								       class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>

						<hr>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='upBtn' name='upBtn' class='btn btn-primary'>上传</button>
					</div>
				</form>
			</div>


		</div>
	</div>

	<!-- 分页部分 -->
	<div class="container-fluid">
		<?php
		// 下一页
		$next_page = 0;
		//上一页
		$pre_page = 0;

		if ($pageNum > 1) {
			$pre_page = $pageNum - 1;
		} else {
			$pre_page = $pageNum;
		}

		if ($pageNum < $page_sum) {
			$next_page = $pageNum + 1;
		} else {
			$next_page = $pageNum;
		}
		?>
		<!-- 分页按钮 -->
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-center">
				<!-- 第一页按钮 -->
				<li class="page-item"><a class="page-link"
				                         href="index.php?courseID=<?= $courseID ?>&page=1&searchInfo=<?= $searchInfo ?>"
				                         target="studentFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- 上一页按钮 -->
				<li class="page-item">
					<a class="page-link"
					   href="index.php?courseID=<?= $courseID ?>&page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>"
					   aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
						<span class="sr-only">Previous</span>
					</a>
				</li>

				<?php
				//循环开始
				for ($i = 1; $i <= $page_sum; $i++) {
					// 是否激活
					$is_active = "";
					if ($i == $pageNum) {
						$is_active = "active";
					}
					?>

					<li class="page-item <?= $is_active ?>">
						<a class="page-link "
						   href="index.php?courseID=<?= $courseID ?>&page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
						</a>
					</li>
					<?php
				} // 循环结束


				?>
				<!-- 下一页按钮 -->
				<li class="page-item">
					<a class="page-link"
					   href="index.php?courseID=<?= $courseID ?>&page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>"
					   aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
						<span class="sr-only">Next</span>
					</a>
				</li>
				<!-- 第最后一页按钮 -->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<li class="page-item"><a class="page-link"
				                         href="index.php?courseID=<?= $courseID ?>&page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
				                         target="studentFrame">尾页</a></li>
			</ul>
		</nav>
		<!-- 分页信息 -->
		<div class="col-12 text-center">
			共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
					class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span
					class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
		</div>
	</div>

	<script>
		let uploadFile = function (id, name) {
			let jobID = document.getElementById("upJobID");
			let jobName = document.getElementById("upJobName");
			jobID.value = id;
			jobName.value = name;
		}
	</script>

<?php require_once '../../base/footer-iframe.php' ?>