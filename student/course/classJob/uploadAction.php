<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../sql/connection.php';

$jobID = $_POST['upJobID'];
$courseID = $_POST['upCourseID'];
$stuID = $_COOKIE['studentID'];
$stuNum = $_COOKIE['studentNum'];
$stuName = $_COOKIE['studentName'];

// 模板文件
if (!empty($_FILES)) {
	$tmpName = $_FILES['templateFile']['tmp_name'];     // 临时文件名称
	// $suffix = explode(".", $_FILES['templateFile']['name']);;         // 文件的后缀
	// 文件名
	$fileName = $stuNum . '-' . $stuName . '-' . '-' . $_FILES['templateFile']['name'];
	// 上传目录
	$path = '/upload/student/'. $courseID . '/' . $jobID . '/';

	// 判断目录是否存在，如果不存在则新建目录
	if (!is_dir('../../..'.$path)) {
		// 创建目录
		mkdir('../../..'.$path, 0777, true);
	}
	
	// $file_name = date('YmdHis').rand(100,999).$name;// 避免文件重名，更改文件名称
	if (move_uploaded_file($tmpName, '../../..'.$path . $fileName)) {
		$now = date('Y-m-d H:i:s', time());
		$answerFile = $path.$fileName;
		$uploadSql = "INSERT INTO `student-job`(create_time, update_time, stu_ID, job_ID, answers, answer_file, is_delete)
						VALUES ('$now','$now','$stuID','$jobID','' ,'$answerFile',0)";
		
		if (mysqli_query($GLOBALS['conn'], $uploadSql)) {
			echo "
            <script>
                alert('作业提交成功！');
                window.location.href=document.referrer;
            </script>
        ";
		} else {
			echo "
            <script>
                alert('作业提交失败！');
                history.back();
            </script>
        ";
		}
	}
} else {
	echo "
            <script>
                alert('文件上传失败！');
                history.back();
            </script>
        ";
	
}

	




