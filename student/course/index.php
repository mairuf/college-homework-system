<?php

// ini_set("display_errors", "On");
// error_reporting(E_ALL);


require_once '../base/header-iframe.php';

require_once '../../sql/connection.php';
?>

	<!-- 主体部分 -->
	<div class="container-fluid h-100">
		<!-- 导航部分信息 -->
		<ul class='nav justify-content-center'>
			<li class='nav-item'>
				<a class='nav-link active' href=''>我的课程列表</a>
			</li>
			<li>
				<form class="form-inline" action="" method="get" id="searchFrom">
					<input class="form-control mr-sm-2" type="search"
					       placeholder="搜索课程" aria-label="Search"
					       id="searchInfo" name="searchInfo"
					>
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
				</form>
			</li>
		</ul>
		<!-- 列表 -->
		<table class='table table-striped text-center'>
			<thead>
			<tr>
				<th scope='col'>课程编号</th>
				<th scope='col'>课程名称</th>
				<th scope='col'>操作</th>
			</tr>
			</thead>
			<tbody>

			<?php
			// 搜索信息，如果没有传入搜索信息，则设为 null
			$searchInfo = $_GET['searchInfo'] ?: null;
			// 页码
			$pageNum = intval($_GET['page'] ?: 1);
			// 数据库索引 ——
			$index = ($pageNum - 1) * 10;
			$classID = $_COOKIE['classID'];
			// 无搜索信息
			if ($searchInfo == null) {
				// 统计学生所在班级的课程总数
				$countSql = "select count(*) as count from course_info where is_delete = 0 and id IN (select course_ID from `course-class` where class_ID = '$classID');";
				$selectSql = "select id, course_number, course_name, is_delete from course_info
								where is_delete = 0
							  	and id IN (select course_ID from `course-class` where class_ID = '$classID')
								limit $index, 10;";
			} // 有搜索信息
			else {
				$countSql = "select count(*) as count from course_info where is_delete = 0 and id IN (select course_ID from `course-class` where class_ID = '$classID')
								and  course_info.course_name like '%$searchInfo%';";

				$selectSql = "select id, create_time, update_time, course_number, course_name, is_delete from course_info where is_delete = 0
                           		and id IN (select course_ID from `course-class` where class_ID = '$classID')
                              	and course_info.course_name like '%$searchInfo%' limit $index, 10;";
			}
			// 连接数据库，并查询
			$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
			// 获取课程总数
			$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
			// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
			$page_sum = intval(ceil($count['count'] / 10));

			// 循环输出所有查询结果
			while ($row = mysqli_fetch_array($resultConfig)) {
				?>


				<tr>
					<td><?= $row['course_number'] ?></td>
					<td><?= $row['course_name'] ?></td>
					<td>
						<a class='btn btn-outline-primary' href="classJob/index.php?courseID=<?= $row['id'] ?>" target="studentFrame">
							查看作业
						</a>
					</td>
				</tr>

				<?php
			};

			?>
			</tbody>
		</table>
	</div>
	<!-- 分页部分 -->
	<div class="container-fluid">
		<?php
		// 下一页
		$next_page = 0;
		//上一页
		$pre_page = 0;

		if ($pageNum > 1) {
			$pre_page = $pageNum - 1;
		} else {
			$pre_page = $pageNum;
		}

		if ($pageNum < $page_sum) {
			$next_page = $pageNum + 1;
		} else {
			$next_page = $pageNum;
		}
		?>
		<!-- 分页按钮 -->
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-center">
				<!-- 第一页按钮 -->
				<li class="page-item"><a class="page-link" href="index.php?page=1&searchInfo=<?= $searchInfo ?>"
				                         target="studentFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- 上一页按钮 -->
				<li class="page-item">
					<a class="page-link" href="index.php?page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>"
					   aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
						<span class="sr-only">Previous</span>
					</a>
				</li>

				<?php
				//循环开始
				for ($i = 1; $i <= $page_sum; $i++) {
					// 是否激活
					$is_active = "";
					if ($i == $pageNum) {
						$is_active = "active";
					}
					?>

					<li class="page-item <?= $is_active ?>">
						<a class="page-link "
						   href="index.php?page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
						</a>
					</li>
					<?php
				} // 循环结束


				?>
				<!-- 下一页按钮 -->
				<li class="page-item">
					<a class="page-link" href="index.php?page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>"
					   aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
						<span class="sr-only">Next</span>
					</a>
				</li>
				<!-- 第最后一页按钮 -->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<li class="page-item"><a class="page-link"
				                         href="index.php?page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
				                         target="studentFrame">尾页</a></li>
			</ul>
		</nav>
		<!-- 分页信息 -->
		<div class="col-12 text-center">
			共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
					class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span
					class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
		</div>
	</div>

	<!-- JavaScript代码 -->
	<script>

		let search = document.getElementById("searchInfo");

		search.value = "<?= $searchInfo ?>";

		let update = function (id, name) {
			let editID = document.getElementById("editID");
			let editName = document.getElementById("editName");
			console.log(editID);

			editID.value = id;
			editName.value = name;

		};
		let deletePer = function (id, name) {
			let deleteInfo = document.getElementById("deleteInfo");
			let deleteID = document.getElementById("deleteID");

			console.log(id);
			console.log(deleteID);
			deleteInfo.innerText = name;
			deleteID.value = id;
		}


	</script>


<?php require_once '../base/footer-iframe.php' ?>