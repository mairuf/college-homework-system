<?php

// ini_set("display_errors", "On");
// error_reporting(E_ALL);

require_once '../base/header-iframe.php';

require_once '../../sql/connection.php';


$answers = $_POST;
$jobID = $_POST['jobID'];
$stuID = $_COOKIE['studentID'];

// 第一个表单值是作业ID，删除，只保留答案
array_shift($answers);
$answerStr = '学生答案（按题目顺序）：';
// 遍历答案，并将答案拼接在一起，然后存储到数据库中，老师只关注答案，不用关注问题
foreach ($answers as &$answer) {
	if ($answer == '') {
		$answerStr = $answerStr . "未填写答案";
	} else {
		$answerStr = $answerStr . $answer;
	}
	$answerStr = $answerStr . '；';
}

$now = date('Y-m-d H:i:s', time());
$insertSql = "INSERT INTO `student-job` (create_time, update_time, stu_ID, job_ID, answers, is_delete) VALUES ('$now','$now','$stuID','$jobID','$answerStr',0)";

if (mysqli_query($GLOBALS['conn'], $insertSql)) {
	echo "
            <script>
                alert('提交成功！即将返回课程列表');
                window.location.href='../course/index.php';
            </script>
        ";
} else {
	echo "
            <script>
                alert('提交失败！');
                history.back();
            </script>
        ";
}