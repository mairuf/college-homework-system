<?php

// ini_set("display_errors", "On");
// error_reporting(E_ALL);

require_once '../base/header-iframe.php';

require_once '../../sql/connection.php';
$jobID = $_GET['jobID'];
$stuID = $_COOKIE['studentID'];

// 查询语句
$querySql = "select score,answers from `student-job` where is_delete = 0 and job_ID = '$jobID' and stu_ID = '$stuID';";
// 连接数据库，并查询
$resultConfig = mysqli_query($GLOBALS['conn'], $querySql);
// 获取学生答案
$row = mysqli_fetch_array($resultConfig);
$answers = substr($row['answers'], 36);
$score = $row['score'];
?>
	<div class="container-fluid h-100">
		<ul class='nav justify-content-center'>
			<li class='nav-item'>
				<a class='nav-link active' href=''>刷新界面</a>
			</li>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</ul>
		<!-- 以卡片的形式展示题目 -->
		<div class="card text-center">
			<div class="card-header" style="background: rgba(63,167,220,0.82)">
				<h5 class="card-title">您的上次提交的答案（按题目顺序）：</h5>
			</div>
			<!-- 学生答案 -->
			<div class="card-body" style="background: rgba(70,168,122,0.82)">
				<h5 class="card-title"><?= $answers ?></h5>
				<p class="card-text"></p>
			</div>
			<div class="card-header" style="background: rgba(255,255,255,0.82)">
				<h5 class="card-title">本次作业得分：<span style="color: #00c3e1;font-size: 30px"><?= $score ?></span></h5>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-6">
				<?php
				// 查询语句
				$querySql = "select id, create_time, update_time, bank_ID, question_title, question_description,
       							question_type, question_opt_A, question_opt_B, question_opt_C, question_opt_D,
       							question_opt_answer, question_completion_answer, is_delete from question_info
       							where is_delete = 0
       							and id IN (select question_ID from `job-question` where job_ID = '$jobID');";
				// 连接数据库，并查询
				$resultConfig = mysqli_query($GLOBALS['conn'], $querySql);
				$num = 1;
				// 循环输出所有查询结果
				while ($row = mysqli_fetch_array($resultConfig)) {
					?>
					<!-- 题目列表	-->
					<div class='form-group row justify-content-center'>

						<div class='col-8'>
							<?php
							// 如果题目类型 == 1，代表改题目为选择题
							if ($row['question_type'] == 1) {
								?>
								<!-- 以卡片的形式展示题目 -->
								<div class="card text-center">
									<!-- 题目标题 -->
									<div class="card-header" style="background: rgba(63,167,220,0.82)">
										<h5 class="card-title">第&nbsp;&nbsp;<?= $num ?>&nbsp;&nbsp;题（选择题）</h5>
									</div>
									<!-- 题目描述 -->
									<div class="card-body">
										<h5 class="card-title">请回答问题：<?= $row['question_description'] ?></h5>
										<p class="card-text"></p>
									</div>
									<!-- 选择题选项 -->
									<div class="card-footer " style="background:rgba(204,204,204,0.82)">
										<h5>选项</h5>
										<div class="row justify-content-center ">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" disabled
												       name="<?= $num . '_' . 'answer' ?>"
												       id="<?= $num . '_' . 'opt_A' ?>"
												       value="A">
												<h5>A.&nbsp;&nbsp;<label class="form-check-label"
												                         for="<?= $num . '_' . 'opt_A' ?>"><?= $row['question_opt_A'] ?></label>
												</h5>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" disabled
												       name="<?= $num . '_' . 'answer' ?>"
												       id="<?= $num . '_' . 'opt_B' ?>"
												       value="B">
												<h5>B.&nbsp;&nbsp;<label class="form-check-label"
												                         for="<?= $num . '_' . 'opt_B' ?>"><?= $row['question_opt_B'] ?></label>
												</h5>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" disabled
												       name="<?= $num . '_' . 'answer' ?>"
												       id="<?= $num . '_' . 'opt_C' ?>"
												       value="C">
												<h5>C.&nbsp;&nbsp;<label class="form-check-label"
												                         for="<?= $num . '_' . 'opt_C' ?>"><?= $row['question_opt_C'] ?></label>
												</h5>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" disabled
												       name="<?= $num . '_' . 'answer' ?>"
												       id="<?= $num . '_' . 'opt_D' ?>"
												       value="D">
												<h5>D.&nbsp;&nbsp;<label class="form-check-label"
												                         for="<?= $num . '_' . 'opt_D' ?>"><?= $row['question_opt_D'] ?></label>
												</h5>
											</div>
										</div>
									</div>
								</div>
								<br><br>


								<?php
							} // 如果题目类型 == 2，代表改题目为填空题
							else if ($row['question_type'] == 2) {
								?>
								<!-- 以卡片的形式展示题目 -->
								<div class="card text-center">
									<!-- 题目标题 -->
									<div class="card-header" style="background: rgba(63,167,220,0.82)">
										<h5 class="card-title">第&nbsp;&nbsp;<?= $num ?>&nbsp;&nbsp;题（填空题）</h5>
									</div>
									<!-- 题目描述 -->
									<div class="card-body">
										<h5 class="card-title">请回答问题：<?= $row['question_description'] ?></h5>
										<p class="card-text"></p>
									</div>
									<!-- 选择题选项 -->
									<div class="card-footer " style="background: rgba(204,204,204,0.82)">
										<h5>
											<label class="form-check-label"
											       for="<?= $num . '_' . 'answer' ?>">请在下面输入框中输入答案
											</label>
										</h5>
										<div class="row justify-content-center ">
											<input type="text" id="<?= $num . '_' . 'answer' ?>" disabled
											       name="<?= $num . '_' . 'answer' ?>">
										</div>
									</div>
								</div>
								<?php
							} // 如果题目类型 == 3，代表改题目为简答题
							else if ($row['question_type'] == 3) {
								?>
								<!-- 以卡片的形式展示题目 -->
								<div class="card text-center">
									<!-- 题目标题 -->
									<div class="card-header" style="background: rgba(63,167,220,0.82)">
										<h5 class="card-title">第&nbsp;&nbsp;<?= $num ?>&nbsp;&nbsp;题（简答题）</h5>
									</div>
									<!-- 题目描述 -->
									<div class="card-body">
										<h5 class="card-title">请回答问题：<?= $row['question_description'] ?></h5>
										<p class="card-text"></p>
									</div>
									<!-- 选择题选项 -->
									<div class="card-footer " style="background: rgba(204,204,204,0.82)">
										<h5>
											<label class="form-check-label"
											       for="<?= $num . '_' . 'answer' ?>">请在下面输入框中输入答案
											</label>
										</h5>
										<textarea id="<?= $num . '_' . 'answer' ?>" disabled></textarea>
									</div>
								</div>
								<?php
							}
							?>
						</div>
					</div>
					<?php
					$num++;
				}

				?>
			</div>
			<div class="col-5">
				<div class="card">
					<form action="commentAction.php" name="commentForm" method="post">
						<div class="card-body" style="background: rgba(255,255,255,0)">
							<input type="text" id="jobID" name="jobID" value="<?=$jobID?>" class="hiddenElement">
							<div class="input-group">
								<textarea class="form-control" id="content" name="content" aria-label="With textarea" maxlength="200" placeholder="请输入评论……（最多200个字符）"></textarea>
							</div>
						</div>
						<div class="card-footer" style="background: rgb(255,255,255)">
							<div class="row justify-content-end">
								<button type='submit' id='btn' name='btn' class='btn btn-primary'>发表评论</button>
							</div>
						</div>
					</form>
				</div>
				<br><br>
				<?php
				// 查询语句
				$querySql = "select id, create_time, update_time, is_delete, jobID, stu_ID, stu_name, content from comment
       							where is_delete = 0
       							and jobID = '$jobID';";
				// 连接数据库，并查询
				$resultConfig = mysqli_query($GLOBALS['conn'], $querySql);
				// 循环输出所有查询结果
				while ($row = mysqli_fetch_array($resultConfig)) {
					?>
					<!-- 题目列表	-->
					<div class="card">
						<div class="card-header" style="background: #00f6c9">
							<h5><?=$row['stu_name']?>（ <?=$row['stu_ID']?> ）</h5>
							<span>发表于 <?=$row['create_time'] ?></span>
						</div>
						<div class="card-body">
							<h5 class="card-title"><?= $row['content']?></h5>
						</div>
					</div>
					<hr>
					<?php
				}

				?>
				
			</div>
		</div>
	</div>
<?php require_once '../base/footer-iframe.php' ?>