<?php

// ini_set("display_errors", "On");
// error_reporting(E_ALL);

require_once '../base/header-iframe.php';

require_once '../../sql/connection.php';
$jobID = $_GET['jobID'];
$jobName = $_GET['jobName'];
?>
	<div class="container-fluid h-100">
		
		<form action="action.php" method="post" name="answerFrom" id="answerFrom"
		      enctype="application/x-www-form-urlencoded">
			<!-- 导航部分信息 -->
			<ul class='nav justify-content-center'>
				<li class='nav-item'>
					<a class='nav-link active' href=''>刷新界面</a>
				</li>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<li>
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">提交做题结果</button>
				</li>

			</ul>
			<div class='row hiddenElement'>
				<input type="text" class='nav-link active' id="jobID" name="jobID" value="<?= $jobID ?>">
				<label class="form-check-label"
				       for="jobID">作业ID</label>
			</div>
			<div class="row justify-content-center"><h3><?=$jobName?></h3></div>

			<?php

			// 查询语句
			$querySql = "select id, create_time, update_time, bank_ID, question_title, question_description,
       							question_type, question_opt_A, question_opt_B, question_opt_C, question_opt_D,
       							question_opt_answer, question_completion_answer, is_delete from question_info
       							where is_delete = 0
       							and id IN (select question_ID from `job-question` where job_ID = '$jobID');";
			// 连接数据库，并查询
			$resultConfig = mysqli_query($GLOBALS['conn'], $querySql);
			$num = 1;
			// 循环输出所有查询结果
			while ($row = mysqli_fetch_array($resultConfig)) {
				?>
				<!-- 题目列表	-->
				<div class='form-group row justify-content-center'>

					<div class='col-4'>
						<?php
						// 如果题目类型 == 1，代表改题目为选择题
						if ($row['question_type'] == 1) {
							?>
							<!-- 以卡片的形式展示题目 -->
							<div class="card text-center">
								<!-- 题目描述 -->
								<div class="card-body" style="background: rgba(63,167,220,0.82)">
									<h5 class="card-title"><?= $num ?>. <?= $row['question_description'] ?></h5>
									<p class="card-text"></p>
								</div>
								<!-- 选择题选项 -->
								<div class="card-footer " style="background: rgba(70,168,122,0.82)">
									<h5>选项</h5>
									<div class="row justify-content-center ">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
											       name="<?= $num . '_' . 'answer' ?>" id="<?= $num . '_' . 'opt_A' ?>"
											       value="A">
											<h5>A.&nbsp;&nbsp;<label class="form-check-label"
											                         for="<?= $num . '_' . 'opt_A' ?>"><?= $row['question_opt_A'] ?></label>
											</h5>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
											       name="<?= $num . '_' . 'answer' ?>" id="<?= $num . '_' . 'opt_B' ?>"
											       value="B">
											<h5>B.&nbsp;&nbsp;<label class="form-check-label"
											                         for="<?= $num . '_' . 'opt_B' ?>"><?= $row['question_opt_B'] ?></label>
											</h5>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
											       name="<?= $num . '_' . 'answer' ?>" id="<?= $num . '_' . 'opt_C' ?>"
											       value="C">
											<h5>C.&nbsp;&nbsp;<label class="form-check-label"
											                         for="<?= $num . '_' . 'opt_C' ?>"><?= $row['question_opt_C'] ?></label>
											</h5>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
											       name="<?= $num . '_' . 'answer' ?>" id="<?= $num . '_' . 'opt_D' ?>"
											       value="D">
											<h5>D.&nbsp;&nbsp;<label class="form-check-label"
											                         for="<?= $num . '_' . 'opt_D' ?>"><?= $row['question_opt_D'] ?></label>
											</h5>
										</div>
										<div class="form-check form-check-inline hiddenElement">
											<input class="form-check-input" type="radio"
											       name="<?= $num . '_' . 'answer' ?>" id="<?= $num . '_' . 'opt_X' ?>"
											       value="未选择" checked>
											<h5>D.&nbsp;&nbsp;<label class="form-check-label"
											                         for="<?= $num . '_' . 'opt_X' ?>"><?= $row['question_opt_D'] ?></label>
											</h5>
										</div>
									</div>
								</div>
							</div>
							<br>
							<?php
						} // 如果题目类型 == 2，代表改题目为填空题
						else if ($row['question_type'] == 2) {
							?>
							<!-- 以卡片的形式展示题目 -->
							<div class="card text-center">

								<!-- 题目描述 -->
								<div class="card-body" style="background: rgba(63,167,220,0.82)">
									<h5 class="card-title"><?= $num ?>. <?= $row['question_description'] ?></h5>
									<p class="card-text"></p>
								</div>
								<!-- 选择题选项 -->
								<div class="card-footer " style="background: rgba(70,168,122,0.82)">
									<h5>
										<label class="form-check-label"
										       for="<?= $num . '_' . 'answer' ?>">请在下面输入框中输入答案
										</label>
									</h5>
									<div class="row justify-content-center ">
										<input type="text" id="<?= $num . '_' . 'answer' ?>"
										       name="<?= $num . '_' . 'answer' ?>">
									</div>
								</div>
							</div>
							<?php
						} // 如果题目类型 == 3，代表改题目为简答题
						else if ($row['question_type'] == 3) {
							?>
							<!-- 以卡片的形式展示题目 -->
							<div class="card text-center">
								<!-- 题目描述 -->
								<div class="card-body" style="background: rgba(63,167,220,0.82)">
									<h5 class="card-title"><?= $num ?>. <?= $row['question_description'] ?></h5>
									<p class="card-text"></p>
								</div>
								<!-- 选择题选项 -->
								<div class="card-footer " style="background: rgba(70,168,122,0.82)">
									<h5>
										<label class="form-check-label"
										       for="<?= $num . '_' . 'answer' ?>">请在下面输入框中输入答案
										</label>
									</h5>
									<textarea id="<?= $num . '_' . 'answer' ?>" name="<?= $num . '_' . 'answer' ?>"></textarea>
								</div>
							</div>
							<?php
						}
						?>
					</div>
				</div>
				<?php
				$num++;
			}
			?>
		</form>
	</div>
<?php require_once '../base/footer-iframe.php' ?>