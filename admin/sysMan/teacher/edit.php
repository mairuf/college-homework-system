<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../sql/connection.php';

$tName = $_POST['editName'];
$tEmail = $_POST['editEmail'];
$tPhone = $_POST['editPhone'];
$tID = $_POST['editTID'];
$id = $_POST['editID'];

// 查询语句
$querySql = "select id, create_time, update_time, t_ID, t_name, t_email, t_phone, pwd, is_delete from teacher_info where t_ID='$tID'";
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);
// 查询当前传过来的用户名对应的管理员信息
$adminObj = mysqli_fetch_object($result);

if (mysqli_num_rows($result) > 0 && ($adminObj->id != $id)) {
    echo "
<script>
    alert('该工号已被占用！请重新输入！');
    history.go(-1);
</script>";
} else {
    $now = date('Y-m-d H:i:s', time());
    $editSql = "UPDATE teacher_info set update_time = '$now', t_ID = '$tID', t_name = '$tName', t_email = '$tEmail', t_phone = '$tPhone' where id = '$id'";
    if (mysqli_query($GLOBALS['conn'], $editSql)) {
        echo "
            <script>
                alert('修改成功！');
                window.location.href=document.referrer;
            </script>
        ";
    } else {
        echo "
            <script> 
                alert('修改失败！');
                history.back();
            </script>
        ";
    }

}

