<?php
// 引入头部文件
require_once '../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../sql/connection.php';

$classID = $_GET['classID'];
//phpinfo();
?>

<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>学生列表</a>
		</li>
		<li class='nav-item'>
			<a class='nav-link' href='#' data-toggle='modal' data-target='#add'>添加</a>
		</li>
		<li class='nav-item'>
			<a class='nav-link' href='#' data-toggle='modal' data-target='#batch'>批量操作</a>
		</li>
		<li>
			<form class="form-inline" action="" method="get" id="searchFrom">
				<input class="form-control mr-sm-2" type="search"
				       placeholder="搜索学生" aria-label="Search"
				       id="searchInfo" name="searchInfo"
				>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
				<div class='form-group row hiddenElement'>
					<label for='classID' class='col-3 col-form-label'>班级ID</label>
					<div class='col-sm-8'>
						<input type='text' id='classID' name='classID' class='form-control mx-sm-3'
						       aria-describedby='nameHelpInline' required='required' value="<?=$classID?>"
						>
					</div>
				</div>
			</form>
		</li>

	</ul>

	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>创建时间</th>
			<th scope='col'>最后一次修改时间</th>
			<th scope='col'>学生学号</th>
			<th scope='col'>学生姓名</th>
			<th scope='col'>认证状态</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo'] ?: null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		if ($searchInfo == null) {
			// 统计管理员总数
			$countSql = "select count(*) as count from student_info where is_delete = 0 and class_ID = '$classID' ;";
			$selectSql = "select id, create_time, update_time, stu_ID, stu_name, stu_pwd, class_ID, is_auth, is_delete from student_info
                         	where is_delete = 0 and class_ID = '$classID' limit $index, 10;";
		} // 有搜索信息
		else {
			$countSql = "select count(*) as count from student_info where is_delete = 0 and class_ID = '$classID' and (student_info.stu_name like '%$searchInfo%' or stu_ID like '%$searchInfo%');";
			$selectSql = "select id, create_time, update_time, stu_ID, stu_name, stu_pwd, class_ID, is_auth, is_delete from student_info
						  where is_delete = 0 and class_ID = '$classID' and (student_info.stu_name like '%$searchInfo%' or stu_ID like '%$searchInfo%' )limit $index, 10;";
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			// 判断是否是超管
			if ($row['is_auth'] == 0) {
				$authState = '待验证';
			} else {
				$authState = '已验证';
			}
			?>


			<tr>
				<td><?= $row['create_time'] ?></td>
				<td><?= $row['update_time'] ?></td>
				<td><?= $row['stu_ID'] ?></td>
				<td><?= $row['stu_name'] ?></td>
				<td><?= $authState ?></td>
				<td>
					<?php
					if ($row['is_auth'] == 0) {
						?>
						<button class='btn btn-outline-primary' data-toggle='modal' data-target='#authY'
						        onclick='authYFun(<?= $row['id'] ?>,"<?= $row['stu_ID'] ?>", "<?= $row['stu_name'] ?>")'>
							通过认证
						</button>
						<button class='btn btn-outline-primary' data-toggle='modal' data-target='#authN'
						        onclick='authNFun(<?= $row['id'] ?>,"<?= $row['stu_ID'] ?>", "<?= $row['stu_name'] ?>")'>
							驳回认证
						</button>
						<?php
					}
					?>
					<button class='btn btn-outline-primary' data-toggle='modal' data-target='#edit'
					        onclick='update(<?= $row['id'] ?>,"<?= $row['stu_ID'] ?>", "<?= $row['stu_name'] ?>")'>编辑
					</button>
					<button class='btn btn-outline-danger' data-toggle='modal' data-target='#delete'
					        onclick='deleteStu(<?= $row['id'] ?>, "<?= $row['stu_name'] ?>")'>删除
					</button>
				</td>
			</tr>

			<?php
		};

		?>
		</tbody>
	</table>

	<!-- 添加部件 -->
	<div class='modal fade bd-example-modal-lg' id='add' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>添加学生</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='add.php' method='post' target='adminFrame' name="addForm" id="addForm">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='classID' class='col-3 col-form-label'>学生学号</label>
							<div class='col-sm-8'>
								<input type='text' id='classID' name='classID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required' value="<?= $classID ?>"
								>
							</div>
						</div>
						<div class='form-group row'>
							<label for='addStuID' class='col-3 col-form-label'>学生学号</label>
							<div class='col-sm-8'>
								<input type='text' id='addStuID' name='addStuID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='addStuName' class='col-3 col-form-label'>学生名称</label>
							<div class='col-sm-8'>
								<input type='text' id='addStuName' name='addStuName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>

					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='addBtn' name='addBtn' class='btn btn-primary'>添加</button>
					</div>
				</form>
			</div>


		</div>
	</div>
	<!-- 批量操作部件 -->
	<div class='modal fade bd-example-modal-lg' id='batch' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>批量操作</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='batchAction.php' method='post' target='adminFrame' name="batchForm" id="batchForm"
				      enctype="multipart/form-data">
					<div class='modal-body'>
						<div class='form-group row justify-content-center'>
							<a class='btn btn-primary' href="../../../assets/学生批量管理模板.xls">下载模板文件</a>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='batchFile' class='col-3 col-form-label'>学生信息（Excel文件）</label>
							<div class='col-sm-8'>
								<input type='file' id='batchFile' name='batchFile' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
						<span class="h5">操作类型</span><br>
						<div class='form-group row '>
							<div class='col-sm-12'>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="type1" name="batchType" class="custom-control-input"
									       value="1" checked>
									<label class="custom-control-label" for="type1">新增</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="type2" name="batchType" class="custom-control-input"
									       value="2">
									<label class="custom-control-label" for="type2">修改</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="type3" name="batchType" class="custom-control-input"
									       value="3">
									<label class="custom-control-label" for="type3">删除</label>
								</div>
							</div>
						</div>
					</div>
					<span class="h4" style="color: red">注意：批量删除操作会删除学生数据，此操作不可逆（不可恢复数据），请谨慎操作！！！！！！！！！！！！！！！！！</span>
					<br><br>
					<div class='form-group row hiddenElement'>
						<label for='classID' class='col-3 col-form-label'>班级ID</label>
						<div class='col-sm-8'>
							<input type='text' id='classID' name='classID' class='form-control mx-sm-3'
							       aria-describedby='nameHelpInline' required='required' value="<?= $classID ?>"
							>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='addBtn' name='addBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>
			</div>


		</div>
	</div>
	<!-- 编辑部件 -->
	<div class='modal fade ' id='edit' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content '>
				<div class='modal-header'>
					<h5 class='modal-title ' id='exampleModalCenterTitle'>编辑学生信息</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='edit.php' method='post' target='adminFrame' name="editForm" id="editForm">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='editID' class='col-3 col-form-label '>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id='editID' name='editID' class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
						<div class='form-group row'>
							<label for='editStuID' class='col-3 col-form-label'>学生学号</label>
							<div class='col-sm-8'>
								<input type='text' id='editStuID' name='editStuID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='editStuName' class='col-3 col-form-label'>学生名称</label>
							<div class='col-sm-8'>
								<input type='text' id='editStuName' name='editStuName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='editBtn' name='editBtn' class='btn btn-primary'>更改</button>
					</div>
				</form>
			</div>

		</div>
	</div>
	<!-- 删除部件 -->
	<div class='modal fade' id='delete' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>删除学生</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action="delete.php" id="deleteForm" name="deleteForm" method="post">
					<div class='modal-body'>
						<p>
							是否确认删除名为：”
							<span id="deleteInfo" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ 的学生？
						</p>
						<div class='form-group row hiddenElement'>
							<label for='deleteID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id="deleteID" name="deleteID" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='deleteBtn' name='deleteBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>

			</div>
		</div>
	</div>
	<!-- 通过验证部件 -->
	<div class='modal fade' id='authY' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>通过验证</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action="registerAuth.php" id="authYForm" name="authYForm" method="post">
					<div class='modal-body'>
						<p>
							是否确认通过学号为：”<span id="authYNum" class="text-center text-success font-weight-bold"
							                 style="font-size: 30px"></span>“
							<br>
							名为：”
							<span id="authYName" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ 的学生的注册申请？
						</p>
						<div class='form-group row hiddenElement'>
							<label for='authYID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id="authYID" name="authYID" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='authBtn' name='authBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>

			</div>
		</div>
	</div>
	<!-- 驳回验证部件 -->
	<div class='modal fade' id='authN' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>驳回验证</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action="registerAuth.php" id="authNForm" name="authNForm" method="post">
					<div class='modal-body'>
						<p>
							是否确认驳回学号为：”<span id="authNNum" class="text-center text-success font-weight-bold"
							                 style="font-size: 30px"></span>“
							<br>
							名为：”
							<span id="authNName" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ 的学生的注册申请？
						</p>
						<div class='form-group row hiddenElement'>
							<label for='authNID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id="authNID" name="authNID" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='authBtn' name='authBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link"
			                         href="index.php?classID=<?= $classID ?>&page=1&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link"
				   href="index.php?classID=<?= $classID ?>&page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="index.php?classID=<?= $classID ?>&page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link"
				   href="index.php?classID=<?= $classID ?>&page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link"
			                         href="index.php?classID=<?= $classID ?>&page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let search = document.getElementById("searchInfo");

	search.value = "<?= $searchInfo ?>";

	let update = function (id, stuID, name) {
		let editID = document.getElementById("editID");
		let editStuID = document.getElementById("editStuID");
		let editStuName = document.getElementById("editStuName");
		// console.log(editStuID);

		editID.value = id;
		editStuID.value = stuID;
		editStuName.value = name;

	};
	let deleteStu = function (id, name) {
		let deleteInfo = document.getElementById("deleteInfo");
		let deleteID = document.getElementById("deleteID");

		deleteInfo.innerText = name;
		deleteID.value = id;
	}

	let authYFun = function (id, num, name) {
		let authYID = document.getElementById("authYID");
		let authYNum = document.getElementById("authYNum");
		let authYName = document.getElementById("authYName");

		authYID.value = id;
		authYNum.innerText = num;
		authYName.innerText = name;
	}

	let authNFun = function (id, num, name) {
		let authNID = document.getElementById("authNID");
		let authNNum = document.getElementById("authNNum");
		let authNName = document.getElementById("authNName");

		authNID.value = id;
		authNNum.innerText = num;
		authNName.innerText = name;
	}


</script>

<?php
// 引入尾部文件
require_once '../../base/footer-iframe.php';

?>

