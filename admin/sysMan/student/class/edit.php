<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../../sql/connection.php';

$editID = $_POST['editID'];
$editClassName = $_POST['editClassName'];

// 查询语句
$querySql = "select id, create_time, update_time, name, is_delete from class_info where name='$editClassName'";
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);
// 查询当前传过来的用户名对应的管理员信息
$adminObj = mysqli_fetch_object($result);

if (mysqli_num_rows($result) > 0) {
    echo "
<script>
    alert('该班级名称已被占用！请重新输入！');
    history.go(-1);
</script>";
} else {
    $now = date('Y-m-d H:i:s', time());
    $editSql = "UPDATE class_info set update_time = '$now', name = '$editClassName' where id = '$editID'";
    if (mysqli_query($GLOBALS['conn'], $editSql)) {
        echo "
            <script>
                alert('修改成功！');
                window.location.href=document.referrer;
            </script>
        ";
    } else {
        echo "
            <script>
                alert('修改失败！');
                history.back();
            </script>
        ";
    }

}

