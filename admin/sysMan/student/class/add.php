<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../../sql/connection.php';

$role = $_COOKIE['role'];
$guideId = $_COOKIE['adminID'];
$addClassName = $_POST['addClassName'];


// 查询语句
$querySql = "select id, create_time, update_time, name, is_delete from class_info where name ='$addClassName'";
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);

//echo $result_config;
if (mysqli_num_rows($result) > 0) {
	echo "
<script>
    alert('该班级已存在，请重新输入！');
    history.go(-1);
</script>";
} else {
	$now = date('Y-m-d H:i:s', time());
	if ($role == "导员"){
		$addSql = "INSERT INTO class_info(create_time, update_time, name ,guide_id, is_delete)VALUES ('$now','$now','$addClassName',$guideId,0)";
	}else{
		$addSql = "INSERT INTO class_info(create_time, update_time, name ,is_delete)VALUES ('$now','$now','$addClassName',0)";
	}
	if (mysqli_query($GLOBALS['conn'], $addSql)) {
		echo "
            <script>
                alert('添加成功！');
                window.location.href=document.referrer;
            </script>
        ";
	} else {
		echo "
            <script>
                alert('添加失败！');
                history.back();
            </script>
        ";
	}
	
}

