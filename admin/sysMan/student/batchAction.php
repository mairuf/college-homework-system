<?php
header("content-type:text/html;charset=utf-8");
// ini_set("display_errors", "On");
// error_reporting(E_ALL);

require_once "../../../assets/framework/PHPExcel/PHPExcel/IOFactory.php";
// 引入数据库连接文件
require_once '../../../sql/connection.php';
date_default_timezone_set('PRC');
$addStuPwd = password_hash("123456", PASSWORD_DEFAULT);

$batchType = $_POST['batchType'];
$classID = $_POST['classID'];

// 模板文件
if (!empty($_FILES)) {
	$tmpName = $_FILES['batchFile']['tmp_name'];     // 临时文件名称
	// $suffix = explode(".", $_FILES['templateFile']['name']);;         // 文件的后缀
	// 文件名
	$fileName = $_FILES['batchFile']['name'];
	// 上传目录
	$path = "/upload/tmp/";
	// 文件临时路径
	$bFile = '../../..' . $path . $fileName;
	
	if (move_uploaded_file($tmpName, $bFile)) {
		// 读取excel文件
		try {
			$inputFileType = PHPExcel_IOFactory::identify($bFile);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($bFile);
		} catch (Exception $e) {
			die("加载文件发生错误：" . pathinfo($bFile, PATHINFO_BASENAME) . ":" . $e->getMessage());
		}
		// 读取工作表(0 表示读取第一张工作表)
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow(); // 行数
		$highestColumn = $sheet->getHighestColumn(); // 列数
		$now = date('Y-m-d H:i:s', time());
		
		switch ($batchType) {
			case "1":
				$count = 0;
				for ($row = 2; $row <= $highestRow; $row++) {
					// 将读取的一行数据放入数组中
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					// 获取学号
					$num = $rowData[0][0];
					// 获取姓名
					$name = $rowData[0][1];
					
					if ($num == '' || $name == ''){
						$count++;
						continue;
					}
					
					$addSql = "INSERT INTO student_info(create_time, update_time, stu_ID, stu_name, stu_pwd, class_ID, is_auth ,is_delete)
												VALUES ('$now','$now','$num','$name','$addStuPwd', '$classID' , 1, 0)";
					if (mysqli_query($GLOBALS['conn'], $addSql)) {
						$count++;
					}
					//这里得到的rowData都是一行的数据，得到数据后自行处理，我们这里只打出来看看效果
					// print_r($rowData);
					// print_r($num . "——————————" . $name);
					
					// echo "新增操作";
				}
				if ($count == ($highestRow - 1)) {
					echo "
				            <script>
				                alert('批量添加成功！');
				                window.location.href=document.referrer;
				            </script>
				        ";
				} else {
					echo "
				            <script>
				                alert('批量添加失败！');
				                history.back();
				            </script>
				        ";
				}
				break;
			
			case "2":
				$count = 0;
				for ($row = 2; $row <= $highestRow; $row++) {
					// 将读取的一行数据放入数组中
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					// 获取学号
					$num = $rowData[0][0];
					// 获取姓名
					$name = $rowData[0][1];
					$updateSql = "UPDATE student_info set update_time = '$now' , stu_name = '$name'  where stu_ID = '$num'";
					if (mysqli_query($GLOBALS['conn'], $updateSql)) {
						$count++;
					}
					//这里得到的rowData都是一行的数据，得到数据后自行处理，我们这里只打出来看看效果
					// print_r($rowData);
					// print_r($num . "——————————" . $name);
					
					// echo "修改操作";
				}
				if ($count == ($highestRow - 1)) {
					echo "
				            <script>
				                alert('批量修改成功！');
				                window.location.href=document.referrer;
				            </script>
				        ";
				} else {
					echo "
				            <script>
				                alert('批量修改失败！');
				                history.back();
				            </script>
				        ";
				}
				break;
			
			
			case "3":
				$count = 0;
				for ($row = 2; $row <= $highestRow; $row++) {
					// 将读取的一行数据放入数组中
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					// 获取学号
					$num = $rowData[0][0];
					// 获取姓名
					// $name = $rowData[0][1];
					// 删除操作，为了方便，这里进行物理删除
					$deleteSql = "DELETE FROM student_info WHERE stu_ID = '$num'";
					if (mysqli_query($GLOBALS['conn'], $deleteSql)) {
						$count++;
					}
					//这里得到的rowData都是一行的数据，得到数据后自行处理，我们这里只打出来看看效果
					// print_r($rowData);
					// print_r($num . "——————————" . $name);
					
					// echo "删除操作";
				}
				if ($count == ($highestRow - 1)) {
					echo "
				            <script>
				                alert('批量删除成功！');
				                window.location.href=document.referrer;
				            </script>
				        ";
				} else {
					echo "
				            <script>
				                alert('批量删除失败！');
				                history.back();
				            </script>
				        ";
				}
				break;
		}
		
		
	}
	
	
}
