<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../sql/connection.php';

$id = $_POST['editID'];
$editStuID = $_POST['editStuID'];
$editStuName = $_POST['editStuName'];
// $editStuPwd = $_POST['$editStuPwd'];

// 查询语句
$querySql = "select id, create_time, update_time, stu_ID, stu_name, stu_pwd, is_delete from student_info where stu_ID='$editStuID'";
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);
// 查询当前传过来的用户名对应的管理员信息
$adminObj = mysqli_fetch_object($result);

if (mysqli_num_rows($result) > 0 && ($adminObj->id != $id)) {
    echo "
<script>
    alert('该学号已被占用！请重新输入！');
    history.go(-1);
</script>";
} else {
    $now = date('Y-m-d H:i:s', time());
    $editSql = "UPDATE student_info set update_time = '$now', stu_ID = '$editStuID' , stu_name = '$editStuName'  where id = '$id'";
    if (mysqli_query($GLOBALS['conn'], $editSql)) {
        echo "
            <script>
                alert('修改成功！');
                window.location.href=document.referrer;
            </script>
        ";
    } else {
        echo "
            <script>
                alert('修改失败！');
                history.back();
            </script>
        ";
    }

}

