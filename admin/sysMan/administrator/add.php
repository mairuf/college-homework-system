<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../sql/connection.php';

$addNikName = $_POST['addNikName'];
$addName = $_POST['addName'];
$addPassword = $_POST['addPassword'];

// 加密密码
$hashPwd = password_hash("$addPassword", PASSWORD_DEFAULT);
// 查询语句
$querySql = "select id, create_time, update_time, nikeName, name, pwd, is_super, is_delete from admin_info where name='$addName'";
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);

//echo $result_config;
if (mysqli_num_rows($result) > 0) {
    echo "
<script>
    alert('该用户名已存在，请重新输入！');
    history.go(-1);
</script>";
} else {
    $now = date('Y-m-d H:i:s', time());
    $addSql = "INSERT INTO admin_info(create_time, update_time, nikeName, name, pwd, is_delete)VALUES ('$now','$now','$addNikName', '$addName', '$hashPwd',0)";
    if (mysqli_query($GLOBALS['conn'], $addSql)) {
        echo "
            <script>
                alert('添加成功！');
                window.location.href=document.referrer;
            </script>
        ";
    } else {
        echo "
            <script>
                alert('添加失败！');
                history.back();
            </script>
        ";
    }

}

