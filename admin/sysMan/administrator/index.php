<?php
// 引入头部文件
require_once '../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../sql/connection.php';

//phpinfo();
?>

<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>管理员列表</a>
		</li>
		<li class='nav-item'>
			<a class='nav-link' href='#' data-toggle='modal' data-target='#add'>添加</a>
		</li>
		<li>
			<form class="form-inline" action="index.php" method="get" id="searchFrom">
				<input class="form-control mr-sm-2" type="search"
				       placeholder="搜索管理员" aria-label="Search"
				       id="searchInfo" name="searchInfo"
				>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
			</form>
		</li>
	</ul>

	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>创建时间</th>
			<th scope='col'>最后一次修改时间</th>
			<th scope='col'>昵称</th>
			<th scope='col'>用户名</th>
			<th scope='col'>是否为超级管理员</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		// 搜索信息
		$searchInfo = $_GET['searchInfo']? : null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		if ($searchInfo == null) {
			// 统计管理员总数
			$countSql = "select count(*) as count from admin_info where is_delete = 0;";
			// 查询语句
			$selectSql = "select id, create_time, update_time, nikeName, name, pwd, is_super, is_delete from admin_info where is_delete = 0 limit $index, 10;";
		} // 有搜索信息
		else {
			$countSql = "select count(*) as count from admin_info where is_delete = 0 and nikeName like '%$searchInfo%' or name like '%$searchInfo%';";
			$selectSql = "select id, create_time, update_time, nikeName, name, pwd, is_super, is_delete from admin_info
                      		where is_delete = 0 and nikeName like '%$searchInfo%' or name like '%$searchInfo%' limit $index, 10;";
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			// 判断是否是超管
			if ($row[6] == 0) {
				$is_super = '否';
			} else {
				$is_super = '是';
			}
			?>
			<tr>
				<td><?= $row['create_time'] ?></td>
				<td><?= $row['update_time'] ?></td>
				<td><?= $row['nikeName'] ?></td>
				<td><?= $row['name'] ?></td>
				<td><?= $is_super ?></td>
				<td>
					<button class='btn btn-outline-primary' data-toggle='modal' data-target='#edit'
					        onclick='update(<?= $row['id'] ?>,"<?= $row['nikeName'] ?>", "<?= $row['name'] ?>")'>编辑
					</button>
					<button class='btn btn-outline-danger' data-toggle='modal' data-target='#delete'
					        onclick='deleteAdmin(<?= $row['id'] ?>, "<?= $row['name'] ?>")'>删除
					</button>
				</td>
			</tr>

			<?php
		};
		?>
		</tbody>
	</table>

	<!-- 添加部件 -->
	<div class='modal fade bd-example-modal-lg' id='add' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>添加管理员</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='add.php' method='post' target='adminFrame' name="addForm" id="addForm">
					<div class='modal-body'>
						<div class='form-group row'>
							<label for='addNikName' class='col-3 col-form-label'>昵&nbsp;&nbsp;&nbsp;&nbsp;称：</label>
							<div class='col-sm-8'>
								<input type='text' id='addNikName' name='addNikName' class='form-control mx-sm-3'
								       aria-describedby='nikNameHelpInline' required='required'
								       pattern='[A-Za-z0-9]{3,20}|[\u4e00-\u9fa50-9]{2,10}'
								       oninvalid='setCustomValidity("请输入 3 - 20 位英文字母/数字，或输入 2 - 10 位中文字符/数字作为用户昵称")'>
							</div>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='addName' class='col-3 col-form-label'>用户名：</label>
							<div class='col-sm-8'>
								<input type='text' id='addName' name='addName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								       pattern='[A-Za-z0-9]{3,30}'
								       oninvalid='setCustomValidity("请输入 3 - 15 位英文字母/数字作为用户名")'>
							</div>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='addPassword' class='col-3 col-form-label'>密&nbsp;&nbsp;&nbsp;&nbsp;码：</label>
							<div class='col-sm-8'>
								<input type='password' id='addPassword' name='addPassword' class='form-control mx-sm-3'
								       aria-describedby='passwordHelpInline' required='required'
								       pattern='[A-Za-z0-9]{6,25}'
								       oninvalid='setCustomValidity("请输入 6 - 25 位英文字母/数字作为密码")'>
							</div>
						</div>
						<br><br>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='addBtn' name='addBtn' class='btn btn-primary'>添加</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 编辑部件 -->
	<div class='modal fade ' id='edit' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content '>
				<div class='modal-header'>
					<h5 class='modal-title ' id='exampleModalCenterTitle'>编辑管理员信息</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='edit.php' method='post' target='adminFrame' name="editForm" id="editForm">
					<div class='modal-body'>
						<div class='form-group row'>
							<label for='editID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id='editID' name='editID' class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='editNikName' class='col-3 col-form-label'>昵&nbsp;&nbsp;&nbsp;&nbsp;称：</label>
							<div class='col-sm-8'>
								<input type='text' id='editNikName' name='editNikName' class='form-control mx-sm-3'
								       aria-describedby='nikNameHelpInline' required='required'
								       pattern='[A-Za-z0-9]{3,20}|[\u4e00-\u9fa50-9]{2,10}'
								       oninvalid='setCustomValidity("请输入 3 - 20 位英文字母/数字，或输入 2 - 10 位中文字符/数字作为用户昵称")'>
							</div>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='editName' class='col-3 col-form-label'>用户名：</label>
							<div class='col-sm-8'>
								<input type='text' id='editName' name='editName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline'
								       required='required'
								       pattern='[A-Za-z0-9]{3,30}'
								       oninvalid='setCustomValidity("请输入 3 - 15 位英文字母/数字作为用户名")'>
							</div>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='editPassword' class='col-3 col-form-label'>密&nbsp;&nbsp;&nbsp;&nbsp;码：</label>
							<div class='col-sm-8'>
								<input type='password' id='editPassword' name="editPassword"
								       class='form-control mx-sm-3'
								       aria-describedby='passwordHelpInline'>
							</div>
						</div>
						<br><br>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='editBtn' name='editBtn' class='btn btn-primary'>更改</button>
					</div>
				</form>
			</div>

		</div>
	</div>
	<!-- 删除部件 -->
	<div class='modal fade' id='delete' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>删除管理员</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action="delete.php" id="deleteForm" name="deleteForm" method="post">
					<div class='modal-body'>
						<p>
							是否确认删除用户名为：”
							<span id="deleteInfo" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ 的管理员？
						</p>
						<div class='form-group row hiddenElement'>
							<label for='deleteID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8' >
								<input type='text' id="deleteID" name="deleteID" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
						<div class='form-group row hiddenElement'>
							<label for='deleteName' class='col-3 col-form-label'>用&nbsp;户&nbsp;名：</label>
							<div class='col-sm-8'>
								<input type='text' id="deleteName" name="deleteName" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
						<br><br>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='deleteBtn' name='deleteBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="index.php?page=1&searchInfo=<?= $searchInfo ?>" target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="index.php?page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link" href="index.php?page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let search = document.getElementById("searchInfo");

	search.value = "<?= $searchInfo ?>";
	
	let update = function (id, nikeName, name) {
		let editID = document.getElementById("editID");
		let editName = document.getElementById("editName");
		let editNikName = document.getElementById("editNikName");

		// console.log(editName);
		// console.log(editNikName);

		editID.value = id;
		editNikName.value = nikeName;
		editName.value = name;

	};
	let deleteAdmin = function (id, name) {
		let deleteInfo = document.getElementById("deleteInfo");
		let deleteID = document.getElementById("deleteID");
		let deleteName = document.getElementById("deleteName");

		deleteInfo.innerText = name;
		deleteID.value = id;
		deleteName.value = name;
	}


</script>

<?php
// 引入尾部文件
require_once '../../base/footer-iframe.php';

?>

