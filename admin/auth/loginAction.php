<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../sql/connection.php';

$username = $_POST['username'];
$pwd =  $_POST['pwd'];
$role =  $_POST['role'];

if ($role == '管理员'){
	$querySql = "select id, create_time, update_time, nikeName, name, pwd, is_super, is_delete from admin_info where name='$username'";
}
else if($role == '教师'){
	$querySql = "select id, create_time, update_time, t_ID, t_name, t_email, t_phone, pwd, is_delete from teacher_info where t_ID='$username'";
}
else {
	$querySql = "select id, create_time, update_time, is_delete, g_id, g_name, g_email, g_phone, pwd from guide where g_id='$username'";
}
// 查询语句
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);

//echo $result_config;
if (mysqli_num_rows($result) <= 0) {
	echo "
<script>
    alert('无该管理员、教师或导员账户！');
    history.go(-1);
</script>";
} else {
	$now = date('Y-m-d H:i:s', time());
	while ($row = mysqli_fetch_array($result)){
		if (password_verify($pwd, $row['pwd'])) {
			
			if ($role == '管理员'){
				// 设置cookie来区分用户登录状态
				setcookie("adminName",$row['name'],path: '/');
				// 超管
				if ($row['is_super'] == 1){
					setcookie("isSuper",1 ,path: '/');
				}
			}
			else if($role == '教师'){
				// 设置cookie来区分用户登录状态
				setcookie("adminName",$row['t_name'],path: '/');
			}
			else {
				// 设置cookie来区分用户登录状态
				setcookie("adminName",$row['g_name'],path: '/');
			}
			setcookie("adminID",$row['id'],path: '/');
			setcookie("role", $role,path: '/');
			
			echo "
            <script>
                alert('登录成功！');
                window.location.href='/admin/#';
            </script>
        ";
		} else {
			echo "
            <script>
                alert('登录失败，用户名或密码错误！');
                history.back();
            </script>
        ";
		}
	}
	
	
}

