<?php
// 引入头部文件
require_once '../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../sql/connection.php';

//phpinfo();
$bankID = $_GET['bankID'];
?>

<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>题目列表</a>
		</li>
		<li class='nav-item'>
			<a class='nav-link' href='#' data-toggle='modal' data-target='#add' onclick="activeInput('简答题')">添加</a>
		</li>
		<li>
			<form class="form-inline" action="" method="get" id="searchFrom">
				<input class="form-control mr-sm-2" type="search"
				       placeholder="搜索题目" aria-label="Search"
				       id="searchInfo" name="searchInfo"
				>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
				<input class="form-control mr-sm-2 hiddenElement" type="search"
				       placeholder="搜索题目" aria-label="Search"
				       id="bankID" name="bankID" value="<?=$bankID?>"
				>
			</form>
		</li>
	</ul>

	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>创建时间</th>
			<th scope='col'>最后一次修改时间</th>
			<th scope='col'>标题</th>
			<th scope='col'>题干</th>
			<th scope='col'>类型</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		
		// echo "<script>console.log('$bankID')</script>";
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo']? : null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		if ($searchInfo == null) {
			// 统计管理员总数
			$countSql = "select count(*) as count from question_info where is_delete = 0;";
			$selectSql = "select * from question_info where is_delete = 0 and bank_ID = '$bankID' limit $index, 10;";
		} // 有搜索信息
		else {
			$countSql = "select count(*) as count from question_info where is_delete = 0 and  question_title like '%$searchInfo%';";
			$selectSql = "select * from question_info where is_delete = 0 and question_info.question_title and bank_ID = '$bankID' and question_title like '%$searchInfo%' limit $index, 10;";
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			// 判断是否是超管
			?>
			<tr>
				<td><?= $row['create_time'] ?></td>
				<td><?= $row['update_time'] ?></td>
				<td><?= $row['question_title'] ?></td>
				<td><?= $row['question_description'] ?></td>
				<td>
					<?php
						switch ($row['question_type']) {
							case 1:
								echo "选择题";
								break;
							case 2:
								echo "填空题";
								break;
							case 3:
								echo "简答题";
								break;
						}
					?>
				</td>
				<td>
					<button class='btn btn-outline-primary' data-toggle='modal' data-target='#edit'
					        onclick='update(<?= $row['id'] ?>, "<?= $row['question_title'] ?>", "<?= $row['question_description'] ?>", "<?= $row['question_type'] ?>",
							        "<?= $row['question_opt_A'] ?>", "<?= $row['question_opt_B'] ?>", "<?= $row['question_opt_C'] ?>",
							        "<?= $row['question_opt_D'] ?>", "<?= $row['question_opt_answer'] ?>", "<?= $row['question_completion_answer'] ?>")'>编辑
					</button>
					<button class='btn btn-outline-danger' data-toggle='modal' data-target='#delete'
					        onclick='deletePer(<?= $row['id'] ?>, "<?= $row['question_title'] ?>",  "<?= $row['question_description'] ?>")'>删除
					</button>
				</td>
			</tr>

			<?php
		};

		?>
		</tbody>
	</table>

	<!-- 添加部件 -->
	<div class='modal fade .bd-example-modal-xl' id='add' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered modal-xl' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>添加题目</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='add.php' method='post' target='adminFrame' name="addForm" id="addForm">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='bankID' class='col-3 col-form-label '>bankID</label>
							<div class='col-sm-8'>
								<input type='text' id='bankID' name='bankID' class='form-control mx-sm-3'
								       readOnly='readonly' value="<?= $bankID ?>">
							</div>
						</div>
						<div class='form-row'>
							<div class="form-group col-md-4">
								<label for='addTitle' class='col-8 col-form-label'>标题</label>
								<div class='col-sm-12'>
									<input type='text' id='addTitle' name='addTitle' class='form-control mx-sm-3'
									       aria-describedby='nameHelpInline' required='required'
									>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label for='addDes' class='col-8 col-form-label'>题干</label>
								<div class='col-sm-12'>
								<textarea id='addDes' name='addDes' class='form-control mx-sm-3'
								          aria-describedby='nameHelpInline' required='required'
								></textarea>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label for='addQType' class='col-8 col-form-label'>题目类型</label>
								<div class='col-sm-12'>
									<div class="custom-control custom-radio">
										<input type="radio" id="type1" name="addQType" class="custom-control-input" onclick="activeInput('选择题')" value="1">
										<label class="custom-control-label" for="type1">选择题</label>
									</div>
									<div class="custom-control custom-radio">
										<input type="radio" id="type2" name="addQType" class="custom-control-input" onclick="activeInput('填空题')" value="2">
										<label class="custom-control-label" for="type2">填空题</label>
										
									</div>
									<div class="custom-control custom-radio">
										<input type="radio" id="type3" name="addQType" class="custom-control-input" onclick="activeInput('简答题')" value="3">
										<label class="custom-control-label" for="type3">简答题</label>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<!-- 选项 -->
						<div class='form-row'>
							<!--<label for='addName' class='col-12 col-form-label'>请输入选择题和选题题答案</label>-->
							<div class="form-group col-md-3">
								<label for='addOpt1' class='col-4 col-form-label'>选项1</label>
								<div class='col-sm-11'>
									<textarea type='text' id='addOpt1' name='addOpt1' class='form-control mx-sm-3'
									       aria-describedby='nameHelpInline' required='required' 
									></textarea>
								</div>
							</div>
							<div class="form-group col-md-3">
								<label for='addOpt2' class='col-4 col-form-label'>选项2</label>
								<div class='col-sm-11'>
									<textarea type='text' id='addOpt2' name='addOpt2' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required' 
									></textarea>
								</div>
							</div>
							<div class="form-group col-md-3">
								<label for='addOpt3' class='col-4 col-form-label'>选项3</label>
								<div class='col-sm-11'>
									<textarea type='text' id='addOpt3' name='addOpt3' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required' 
									></textarea>
								</div>
							</div>
							<div class="form-group col-md-3">
								<label for='addOpt4' class='col-4 col-form-label'>选项4</label>
								<div class='col-sm-11'>
									<textarea type='text' id='addOpt4' name='addOpt4' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required' 
									></textarea>
								</div>
							</div>
							
						</div >
						<!-- 选择题答案 -->
						<div class='form-row'>
							<div class="form-group col-md-12">
								<label for='addOptAnswer' class='col-12 col-form-label'>选择题答案</label>
								<div class='col-sm-12'>
									<textarea type='text' id='addOptAnswer' name='addOptAnswer' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required' 
									></textarea>
								</div>
							</div>
						</div>
						<hr>
						<!-- 填空题答案 -->
						<div class="form-group">
							<label for='addComAnswer' class='col-3 col-form-label'>填空题答案</label>
							<div class='col-sm-12'>
								<textarea id='addComAnswer' name='addComAnswer' class='form-control mx-sm-3'
								          aria-describedby='nameHelpInline' required='required' 
								></textarea>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='addBtn' name='addBtn' class='btn btn-primary'>添加</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 编辑部件 -->
	<div class='modal fade ' id='edit' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered modal-xl' role='document'>
			<div class='modal-content '>
				<div class='modal-header'>
					<h5 class='modal-title ' id='exampleModalCenterTitle'>编辑题目信息</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='edit.php' method='post' target='adminFrame' name="editForm" id="editForm">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='editID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id='editID' name='editID' class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
						<div class='form-row'>
							<div class="form-group col-md-4">
								<label for='editTitle' class='col-8 col-form-label'>标题</label>
								<div class='col-sm-12'>
									<input type='text' id='editTitle' name='editTitle' class='form-control mx-sm-3'
									       aria-describedby='nameHelpInline' required='required'
									>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label for='editDes' class='col-8 col-form-label'>题干</label>
								<div class='col-sm-12'>
								<textarea id='editDes' name='editDes' class='form-control mx-sm-3'
								          aria-describedby='nameHelpInline' required='required'
								></textarea>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label for='editQType' class='col-8 col-form-label'>题目类型</label>
								<div class='col-sm-12'>
									<div class="custom-control custom-radio">
										<input type="radio" id="editType1" name="editQType" class="custom-control-input" value="1" onclick="activeInput('选择题')">
										<label class="custom-control-label" for="editType1">选择题</label>
									</div>
									<div class="custom-control custom-radio">
										<input type="radio" id="editType2" name="editQType" class="custom-control-input" value="2" onclick="activeInput('填空题')">
										<label class="custom-control-label" for="editType2">填空题</label>

									</div>
									<div class="custom-control custom-radio">
										<input type="radio" id="editType3" name="editQType" class="custom-control-input"  value="3" onclick="activeInput('简答题')">
										<label class="custom-control-label" for="editType3">简答题</label>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<!-- 选项 -->
						<div class='form-row'>
							<!--<label for='editName' class='col-12 col-form-label'>请输入选择题和选题题答案</label>-->
							<div class="form-group col-md-3">
								<label for='editOpt1' class='col-4 col-form-label'>选项1</label>
								<div class='col-sm-11'>
									<textarea type='text' id='editOpt1' name='editOpt1' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required'
									></textarea>
								</div>
							</div>
							<div class="form-group col-md-3">
								<label for='editOpt2' class='col-4 col-form-label'>选项2</label>
								<div class='col-sm-11'>
									<textarea type='text' id='editOpt2' name='editOpt2' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required'
									></textarea>
								</div>
							</div>
							<div class="form-group col-md-3">
								<label for='editOpt3' class='col-4 col-form-label'>选项3</label>
								<div class='col-sm-11'>
									<textarea type='text' id='editOpt3' name='editOpt3' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required'
									></textarea>
								</div>
							</div>
							<div class="form-group col-md-3">
								<label for='editOpt4' class='col-4 col-form-label'>选项4</label>
								<div class='col-sm-11'>
									<textarea type='text' id='editOpt4' name='editOpt4' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required'
									></textarea>
								</div>
							</div>

						</div >
						<!-- 选择题答案 -->
						<div class='form-row'>
							<div class="form-group col-md-12">
								<label for='editOptAnswer' class='col-12 col-form-label'>选择题答案</label>
								<div class='col-sm-12'>
									<textarea type='text' id='editOptAnswer' name='editOptAnswer' class='form-control mx-sm-3'
									          aria-describedby='nameHelpInline' required='required'
									></textarea>
								</div>
							</div>
						</div>
						<hr>
						<!-- 填空题答案 -->
						<div class="form-group">
							<label for='editComAnswer' class='col-3 col-form-label'>填空题答案</label>
							<div class='col-sm-12'>
								<textarea id='editComAnswer' name='editComAnswer' class='form-control mx-sm-3'
								          aria-describedby='nameHelpInline' required='required'
								></textarea>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='editBtn' name='editBtn' class='btn btn-primary'>更改</button>
					</div>
				</form>
			</div>

		</div>
	</div>
	<!-- 删除部件 -->
	<div class='modal fade' id='delete' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>删除题目</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action="delete.php" id="deleteForm" name="deleteForm" method="post">
					<div class='modal-body'>
						<p>
							是否确认删除标题为：”
							<span id="deleteTitle" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ ，
							题干为：”
							<span id="deleteDes" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ 的题目？
						</p>
						<div class='form-group row hiddenElement'>
							<label for='deleteID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8' >
								<input type='text' id="deleteID" name="deleteID" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='deleteBtn' name='deleteBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="index.php?bankID=<?=$bankID?>&page=1&searchInfo=<?= $searchInfo ?>" target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?bankID=<?=$bankID?>&page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="index.php?bankID=<?=$bankID?>&page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?bankID=<?=$bankID?>&page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link" href="index.php?bankID=<?=$bankID?>&page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>


	let search = document.getElementById("searchInfo");

	search.value = "<?= $searchInfo ?>";
	
	let update = function (id, question_title, question_description, question_type,
	                       question_opt_A, question_opt_B, question_opt_C, question_opt_D,
	                       question_opt_answer, question_completion_answer) {
		let editID = document.getElementById("editID");
		let editTitle = document.getElementById("editTitle");
		let editDes = document.getElementById("editDes");
		let editQType = document.getElementsByName("editQType");
		
		let editOpt1 = document.getElementById("editOpt1");
		let editOpt2 = document.getElementById("editOpt2");
		let editOpt3 = document.getElementById("editOpt3");
		let editOpt4 = document.getElementById("editOpt4");
		let editOptAnswer = document.getElementById("editOptAnswer");
		let editComAnswer = document.getElementById("editComAnswer");
		switch (question_type){
			case '1':
				editQType[0].checked = true;
				activeInput('选择题');
				break;
			case '2':
				editQType[1].checked = true;
				activeInput('填空题');
				break;
			case '3':
				editQType[2].checked = true;
				activeInput('简答题');
				break;
		}
		editID.value = id;
		editTitle.value = question_title;
		editDes.value = question_description;
		editOpt1.value = question_opt_A;
		editOpt2.value = question_opt_B;
		editOpt3.value = question_opt_C;
		editOpt4.value = question_opt_D;
		editOptAnswer.value = question_opt_answer;
		editComAnswer.value = question_completion_answer;


	};
	let deletePer = function (id, title, des) {
		let deleteInfo = document.getElementById("deleteTitle");
		let deleteDes = document.getElementById("deleteDes");
		let deleteID = document.getElementById("deleteID");

		console.log(id);
		console.log(deleteID);
		deleteInfo.innerText = title;
		deleteDes.innerText = des;
		deleteID.value = id;
	}

	let activeInput = function (qType) {
		let textA = document.getElementsByTagName("textarea");
		// console.log(textA)
		switch (qType){
			case '选择题':
				for (let i = 1; i < 6; i++) {
					textA[i].removeAttribute('disabled');
				}
				for (let i = 8; i < 13; i++) {
					textA[i].removeAttribute('disabled');
				}
				textA[6].setAttribute('disabled','disabled');
				textA[13].setAttribute('disabled','disabled');
				break;
			case '填空题':
				for (let i = 1; i < 6; i++) {
					textA[i].setAttribute('disabled','disabled');
				}
				for (let i = 8; i < 13; i++) {
					textA[i].setAttribute('disabled','disabled');
				}
				textA[6].removeAttribute('disabled');
				textA[13].removeAttribute('disabled');
				break;
			case '简答题':
				for (let i = 1; i < 14; i++) {
					textA[i].setAttribute('disabled','disabled');
				}
				textA[7].removeAttribute('disabled');
				break;

		}
	}
</script>

<?php
// 引入尾部文件
require_once '../../base/footer-iframe.php';

?>

