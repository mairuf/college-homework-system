<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../sql/connection.php';

$id = $_POST['editID'];
$editBankNumber = $_POST['editBankNumber'];
$editName = $_POST['editName'];

// 查询语句
$querySql = "select id, create_time, update_time, bank_number, bank_name, bank_total, is_delete from bank_info where bank_number='$editBankNumber'";
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);
// 查询当前传过来的用户名对应的题库信息
$adminObj = mysqli_fetch_object($result);

if (mysqli_num_rows($result) > 0 && ($adminObj->id != $id)) {
    echo "
<script>
    alert('该题库编号已被占用！请重新输入！');
    history.go(-1);
</script>";
} else {
    $now = date('Y-m-d H:i:s', time());
    $editSql = "UPDATE bank_info set update_time = '$now', bank_number = '$editBankNumber' , bank_name = '$editName' where id = '$id'";
    if (mysqli_query($GLOBALS['conn'], $editSql)) {
        echo "
            <script>
                alert('修改成功！');
                window.location.href=document.referrer;
            </script>
        ";
    } else {
        echo "
            <script> 
                alert('修改失败！');
                history.back();
            </script>
        ";
    }

}

