<?php require_once './base/header.php'; ?>
<?php require_once './auth/loginFilter.php'; ?>

<!-- 顶栏 -->
<div class="container-fluid " style="background: rgba(0,183,189,0.62)">
	<nav class="navbar navbar-expand-lg navbar-light">
		<a class="navbar-brand" href="/admin/#">
			<img src="/assets/favicon.png" width="33" height="33" alt="">
		</a>
		<button class="navbar-toggler"
		        type="button"
		        data-toggle="collapse"
		        data-target="#navbarSupportedContent"
		        aria-controls="navbarSupportedContent"
		        aria-expanded="false"
		        aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<!--<li class="nav-item">-->
				<!--	<a class="nav-link" href="/student/#">前台系统</a>-->
				<!--</li>-->
				<li class="nav-item">
					<a class="nav-link" href="javascript:location.reload();">后台管理系统</a>
				</li>
			</ul>
			<?php
			if ($_COOKIE['adminName'] == null || $_COOKIE['adminID'] == null || $_COOKIE['role'] == null) {
				?>
				<div class="dropdown show dropleft">

					<a class="text-white" href="auth/login.php">登录</a>
				</div>
				<?php
			} else {
				?>
				<div class="dropdown show dropleft">
					<a class="dropdown-toggle text-white" href="#" role="button" id="personalCenter"
					   data-toggle="dropdown"
					   aria-haspopup="true" aria-expanded="false">
						<?= $_COOKIE['adminName'] ?>
					</a>
					<div class="dropdown-menu" aria-labelledby="personalCenter">
						<a class="dropdown-item" href="auth/logout.php">退出登录</a>
					</div>
				</div>
				<?php
			}
			?>

		</div>

	</nav>
</div>

<div class="container-fluid h-100">
	<div class="row h-100">
		<!-- 左侧菜单栏 -->
		<nav class="nav col-1 h-100" style="background: #ffffff;">
			<ul class="navbar-nav mr-auto ml-4">
				<!-- 系统管理 -->
				<li class="nav-item dropdown">
					<?php
					if ($_COOKIE['role'] != "教师") {
						?>
						<!-- 展开按钮 -->
						<a class="nav-link"
						   data-toggle="collapse"
						   href="#sysMan"
						   role="button"
						   aria-expanded="false"
						   aria-controls="collapseExample">
							系统管理
						</a>
						<?php
					}
					?>
					
					<!-- 展开项 -->
					<div class="collapse" id="sysMan">
						<?php
						if ($_COOKIE['isSuper'] == 1) {
							?>
							<a class="dropdown-item" href="sysMan/administrator/index.php" target="adminFrame">管理员管理</a>
							<?php
						}
						?>
						<?php
						if ($_COOKIE['role'] == "管理员") {
							?>
							<a class="dropdown-item" href="./sysMan/teacher/index.php" target="adminFrame">教师管理</a>
							<a class="dropdown-item" href="./sysMan/guide/index.php" target="adminFrame">导员管理</a>
							<?php
						}
						?>

						<?php
						if ($_COOKIE['role'] != "教师") {
							?>
							<a class="dropdown-item" href="./sysMan/student/class/index.php" target="adminFrame">学生管理</a>
							<?php
						}
						?>
					</div>
				</li>
				<?php
				if ($_COOKIE['role'] != "导员") {
				?>
				<!-- 题库管理 -->
				<li class="nav-item dropdown">
					<!-- 展开按钮 -->
					<a class="nav-link"
					   data-toggle="collapse"
					   href="#itemBankMan"
					   role="button"
					   aria-expanded="false"
					   aria-controls="collapseExample">
						题库管理
					</a>
					<!-- 展开项 -->
					<div class="collapse" id="itemBankMan">
						
							<a class="dropdown-item" href="bankMan/bank/index.php" target="adminFrame">题库列表</a>
						
						<!--<a class="dropdown-item" href="bankMan/question/index.php" target="adminFrame">题目列表</a>-->
					</div>
				</li>
					<?php
				}
				?>
				<!-- 作业管理 -->
				<li class="nav-item dropdown">
					<!-- 展开按钮 -->
					<a class="nav-link"
					   data-toggle="collapse"
					   href="#jobMan"
					   role="button"
					   aria-expanded="false"
					   aria-controls="collapseExample">
						作业管理
					</a>
					<!-- 展开项 -->
					<div class="collapse" id="jobMan">
						<?php
						if ($_COOKIE['role'] != "导员") {
							?>
							<a class="dropdown-item" href="./jobMan/course/index.php" target="adminFrame">课程管理</a>
							<?php
						}
						?>
						<?php
						if ($_COOKIE['role'] != "教师") {
							?>
							<a class="dropdown-item" href="./jobMan/classCourse/index.php" target="adminFrame">班级课程管理</a>
							<?php
						}
						?>
					</div>
				</li>
			</ul>
		</nav>
		<!-- 右侧内容区 -->
		<div class="col-11 h-100">
			<iframe src="" class="w-100 h-100" name="adminFrame" style="border: 0;"></iframe>
		</div>
	</div>
</div>

<?php require_once './base/footer.php' ?>
