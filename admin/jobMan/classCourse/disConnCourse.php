<?php
// 引入头部文件
require_once '../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../sql/connection.php';
$classId = $_GET['classId'];
//phpinfo();
?>

<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>已关联课程列表</a>
		</li>
		<li>
			<form class="form-inline" action="index.php" method="get" id="searchFrom">
				<input class="form-control mr-sm-2" type="search"
				       placeholder="搜索已关联课程" aria-label="Search"
				       id="searchInfo" name="searchInfo"
				>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
			</form>
		</li>
		<li class='nav-item'>
			<a class='nav-link active' href='connCourse.php?classId=<?=$classId?>'>未关联课程</a>
		</li>
	</ul>

	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>创建时间</th>
			<th scope='col'>最后一次修改时间</th>
			<th scope='col'>课程编号</th>
			<th scope='col'>课程名</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo']? : null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		if ($searchInfo == null) {
			// 统计总数
			$countSql = "select count(*) as count from course_info where is_delete = 0 AND id IN (SELECT course_ID FROM `course-class` WHERE class_ID = '$classId' and is_delete = 0);";
			$selectSql = "select id, create_time, update_time, course_number, course_name, is_delete from course_info where is_delete = 0
 							AND id IN (SELECT course_ID FROM `course-class` WHERE class_ID = '$classId' and is_delete = 0) limit $index, 10;";
		} // 有搜索信息
		else {
			$countSql = "select count(*) as count from course_info where is_delete = 0  AND id IN (SELECT course_ID FROM `course-class` WHERE class_ID = '$classId' and is_delete = 0)
                                            and  course_info.course_name like '%$searchInfo%';";
			$selectSql = "select id, create_time, update_time, course_number, course_name, is_delete from course_info
							where is_delete = 0 AND id IN (SELECT course_ID FROM `course-class` WHERE class_ID = '$classId' and is_delete = 0)
							  and course_info.course_name like '%$searchInfo%' limit $index, 10;";
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			?>
			<tr>
				<td><?= $row['create_time'] ?></td>
				<td><?= $row['update_time'] ?></td>
				<td><?= $row['course_number'] ?></td>
				<td><?= $row['course_name'] ?></td>
				<td>
					<button class='btn btn-outline-primary' data-toggle='modal' data-target='#edit'
					        onclick='update(<?= $row['id'] ?>, "<?= $row['course_name'] ?>")'>取消关联
					</button>
				</td>
			</tr>
			<?php
		};

		?>
		</tbody>
	</table>
	
	<!-- 编辑部件 -->
	<div class='modal fade bd-example-modal-lg' id='edit' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>关联课程</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='edit.php' method='post' target='adminFrame' name="editForm" id="editForm">
					<div class='modal-body'>
						是否确定取消关联课程：<span id='editCourseName' name='editCourseName' class="h3 text-success"></span>

						<div class='form-group row hiddenElement'>
							<label for='editID' class='col-3 col-form-label'>班级ID</label>
							<div class='col-sm-8'>
								<input type='text' id='editID' name='editID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<div class='form-group row hiddenElement'>
							<label for='editClassID' class='col-3 col-form-label'>课程ID</label>
							<div class='col-sm-8'>
								<input type='text' id='editClassID' name='editClassID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required' value="<?= $classId ?>"
								>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='editBtn' name='editBtn' class='btn btn-primary'>取消关联</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="disConnCourse.php?classId=<?=$classId?>&page=1&searchInfo=<?= $searchInfo ?>" target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="disConnCourse.php?classId=<?=$classId?>&page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="disConnCourse.php?classId=<?=$classId?>&page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="disConnCourse.php?classId=<?=$classId?>&page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link" href="disConnCourse.php?classId=<?=$classId?>&page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let search = document.getElementById("searchInfo");

	search.value = "<?= $searchInfo ?>";

	let update = function (id, name) {
		let editID = document.getElementById("editID");
		let editCourseName = document.getElementById("editCourseName");

		editID.value = id;
		editCourseName.innerText = name;

	};


</script>

<?php
// 引入尾部文件
require_once '../../base/footer-iframe.php';

?>

