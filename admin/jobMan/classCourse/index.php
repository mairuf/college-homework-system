<?php
// 引入头部文件
require_once '../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../sql/connection.php';

//phpinfo();
$role = $_COOKIE['role'];
$guideId = $_COOKIE['adminID'];
?>

<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>班级列表</a>
		</li>
		<li class='nav-item'>
			<a class='nav-link' href='#' data-toggle='modal' data-target='#add'>添加</a>
		</li>
		<li>
			<form class="form-inline" action="index.php" method="get" id="searchFrom">
				<input class="form-control mr-sm-2" type="search"
				       placeholder="搜索班级" aria-label="Search"
				       id="searchInfo" name="searchInfo"
				>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
			</form>
		</li>
	</ul>

	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>创建时间</th>
			<th scope='col'>最后一次修改时间</th>
			<th scope='col'>班级名称</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo']? : null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		if ($searchInfo == null) {
			if ($role == "导员"){
				// 统计管理员总数
				$countSql = "select count(*) as count from class_info where is_delete = 0 and guide_id = '$guideId';";
				$selectSql = "select id, create_time, update_time, name, guide_id, is_delete from class_info where is_delete = 0 and guide_id = '$guideId' limit $index, 10;";
			}else{
				// 统计管理员总数
				$countSql = "select count(*) as count from class_info where is_delete = 0;";
				$selectSql = "select id, create_time, update_time, name, guide_id, is_delete from class_info where is_delete = 0 limit $index, 10;";
			}
			
		} // 有搜索信息
		else {
			if ($role == "导员"){
				$countSql = "select count(*) as count from class_info where is_delete = 0 and guide_id = '$guideId'  and class_info.name like '%$searchInfo%';";
				$selectSql = "select id, create_time, update_time, name, is_delete from class_info
						  where is_delete = 0 and guide_id = '$guideId'  and class_info.name like '%$searchInfo%' limit $index, 10;";
			}else{
				$countSql = "select count(*) as count from class_info where is_delete = 0 and class_info.name like '%$searchInfo%';";
				$selectSql = "select id, create_time, update_time, name, is_delete from class_info
						  where is_delete = 0 and class_info.name like '%$searchInfo%' limit $index, 10;";
			}
			
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			// 判断是否是超管
			if ($row[6] == 0) {
				$is_super = '否';
			} else {
				$is_super = '是';
			}
			?>


			<tr>
				<td><?= $row['create_time'] ?></td>
				<td><?= $row['update_time'] ?></td>
				<td><?= $row['name'] ?></td>
				<td>
					<a class='btn btn-outline-primary' href="connCourse.php?classId=<?= $row['id'] ?>">关联课程
					</a>
				</td>
			</tr>

			<?php
		};

		?>
		</tbody>
	</table>

	<!-- 添加部件 -->
	<div class='modal fade bd-example-modal-lg' id='add' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>添加班级</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='../../sysMan/student/class/add.php' method='post' target='adminFrame' name="addClassForm" id="addClassForm">
					<div class='modal-body'>
						<div class='form-group row'>
							<label for='addClassName' class='col-3 col-form-label'>班级名称</label>
							<div class='col-sm-8'>
								<input type='text' id='addClassName' name='addClassName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id="addClassBtn" name="addClassBtn" class='btn btn-primary'>添加</button>
					</div>
				</form>
			</div>


		</div>
	</div>
	<!-- 编辑部件 -->
	<div class='modal fade ' id='edit' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content '>
				<div class='modal-header'>
					<h5 class='modal-title ' id='exampleModalCenterTitle'>编辑学生信息</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='edit.php' method='post' target='adminFrame' name="editForm" id="editForm">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='editID' class='col-3 col-form-label '>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id='editID' name='editID' class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
						<div class='form-group row'>
							<label for='editClassName' class='col-3 col-form-label'>班级名称</label>
							<div class='col-sm-8'>
								<input type='text' id='editClassName' name='editClassName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='editBtn' name='editBtn' class='btn btn-primary'>更改</button>
					</div>
				</form>
			</div>

		</div>
	</div>
	<!-- 删除部件 -->
	<div class='modal fade' id='delete' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>删除班级</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action="delete.php" id="deleteForm" name="deleteForm" method="post">
					<div class='modal-body'>
						<p>
							是否确认删除名为：”
							<span id="deleteInfo" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ 的班级？
						</p>
						<div class='form-group row hiddenElement'>
							<label for='deleteID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8' >
								<input type='text' id="deleteID" name="deleteID" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='deleteBtn' name='deleteBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="index.php?page=1&searchInfo=<?= $searchInfo ?>" target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="index.php?page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link" href="index.php?page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let search = document.getElementById("searchInfo");

	search.value = "<?= $searchInfo ?>";
	
	let update = function (id, name) {
		let editID = document.getElementById("editID");
		let editStuName = document.getElementById("editClassName");
		// console.log(editStuID);

		editID.value = id;
		editStuName.value = name;

	};
	let deleteStu = function (id, name) {
		let deleteInfo = document.getElementById("deleteInfo");
		let deleteID = document.getElementById("deleteID");
		
		deleteInfo.innerText = name;
		deleteID.value = id;
	}


</script>

<?php
// 引入尾部文件
require_once '../../base/footer-iframe.php';

?>

