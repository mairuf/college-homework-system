<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../sql/connection.php';

$courseID = $_POST['editID'];
$classID = $_POST['editClassID'];

// 查询语句
$querySql = $querySql = "select id, create_time, update_time, course_ID, class_ID from `course-class` where class_ID ='$classID' and course_ID = '$courseID'";
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $querySql);
// 查询当前传过来的用户名对应的管理员信息
$adminObj = mysqli_fetch_object($result);

if (mysqli_num_rows($result) < 0) {
    echo "
<script>
    alert('取消关联失败！无该关联关系！');
    history.go(-1);
</script>";
} else {
    $now = date('Y-m-d H:i:s', time());
    $editSql = "UPDATE `course-class` set update_time = '$now',is_delete = 1 where class_ID ='$classID' and course_ID = '$courseID';";
    if (mysqli_query($GLOBALS['conn'], $editSql)) {
        echo "
            <script>
                alert('取消关联成功！');
                window.location.href=document.referrer;
            </script>
        ";
    } else {
        echo "
            <script>
                alert('取消关联失败！');
                history.back();
            </script>
        ";
    }

}

