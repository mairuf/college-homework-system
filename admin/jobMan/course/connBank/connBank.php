<?php
// 引入头部文件
require_once '../../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../../sql/connection.php';

$courseID = $_GET['courseID'];
// $state = $_GET['state'] ?: 0;
// phpinfo();
?>

<!-- 本页面为已关联题库列表 -->
<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item' style="padding: 5px">
			<a class='nav-link active' href=''>题库列表</a>
		</li>
		<li class='nav-item'>
			<form class="form-inline" action="connBank.php" method="get" id="searchFrom">
				<div class="hiddenElement" style="width: 0">
					<label for='courseID' class='col-3 col-form-label'></label>
					<input type='text' id='courseID' name='courseID' class='form-control mx-sm-3'
					       readOnly='readonly' value="<?= $courseID ?>" style="width: 0;">
				</div>
				<div class="hiddenElement" style="width: 0">
					<label for='state' class='col-3 col-form-label'></label>
					<input type='text' id='state' name='state' class='form-control mx-sm-3'
					       readOnly='readonly' value="<?= $courseID ?>" style="width: 0;">
				</div>
				<input class="form-control mr-sm-2" type="search"
				       placeholder="搜索题库" aria-label="Search"
				       id="searchInfo" name="searchInfo"
				>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
			</form>
		</li>&nbsp;&nbsp;&nbsp;&nbsp;
		<li class='nav-item'>
			<div class="btn-group " style="padding: 5px">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
				        aria-haspopup="true" aria-expanded="false">
					已关联
				</button>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="connBank.php?courseID=<?= $courseID ?>&state=1">已关联</a>
					<a class="dropdown-item" href="connBank0.php?courseID=<?= $courseID ?>&state=0">未关联</a>
				</div>
			</div>
		</li>
	</ul>

	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<th scope='col'>题库编号</th>
			<th scope='col'>题库名</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo'] ?: null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;


		// 无搜索信息
		if ($searchInfo == null) {
			// 统计管理员总数
			$countSql = "select count(*) as count from bank_info where is_delete = 0 and id IN (select bank_ID from `course-bank` where course_ID = '$courseID');";
			$selectSql = "select id, create_time, update_time, bank_number, bank_name, bank_total, is_delete from bank_info
							where is_delete = 0 and id IN (select bank_ID from `course-bank` where course_ID = '$courseID' and is_delete = 0) limit $index, 10;";
		} // 有搜索信息
		else {
			$countSql = "select count(*) as count from bank_info where is_delete = 0 and  bank_info.bank_name like '%$searchInfo%'
							and id IN (select bank_ID from `course-bank` where course_ID = '$courseID');";
			$selectSql = "select id, create_time, update_time, bank_number, bank_name, bank_total, is_delete from bank_info
							where is_delete = 0 and bank_info.bank_name like '%$searchInfo%' and id IN (select bank_ID from `course-bank` where course_ID = '$courseID' and is_delete = 0) limit $index, 10;";
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			// 判断是否是超管
			if ($row[6] == 0) {
				$is_super = '否';
			} else {
				$is_super = '是';
			}
			?>


			<tr>
				<td><?= $row['bank_number'] ?></td>
				<td><?= $row['bank_name'] ?></td>
				<td>
					<a class='btn btn-outline-primary' data-toggle='modal' data-target='#connBank'
					   onclick="connBank(<?= $courseID ?>, <?= $row['id'] ?>, '<?= $row['bank_name'] ?>');">
						取消关联题库
					</a>
				</td>
			</tr>

			<?php
		};

		?>
		</tbody>
	</table>

	<!-- 取消关联部件 -->
	<div class='modal fade bd-example-modal-lg' id='connBank' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>关联题库</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<br><br>
				<h5 class="text-center">
					是否确定取消关联题库：<span id="tip" class="text-success h3"></span> ？
				</h5>
				<form action='connBankDelete.php' method='post' target='adminFrame' name="connForm" id="connForm">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='connCouID' class='col-3 col-form-label'>课程ID</label>
							<div class='col-sm-8'>
								<input type='text' id='connCouID' name='connCouID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<div class='form-group row hiddenElement'>
							<label for='connBankID' class='col-3 col-form-label'>题库ID</label>
							<div class='col-sm-8'>
								<input type='text' id='connBankID' name='connBankID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='addBtn' name='addBtn' class='btn btn-primary'>取消关联</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="connBank.php?courseID=<?=$courseID?>&page=1&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="connBank.php?courseID=<?=$courseID?>&page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="connBank.php?courseID=<?=$courseID?>&page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="connBank.php?courseID=<?=$courseID?>&page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link"
			                         href="connBank.php?courseID=<?=$courseID?>&page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let search = document.getElementById("searchInfo");
	search.value = "<?= $searchInfo ?>";

	let connBank = function (couID, bankID, bankName) {
		let connCou = document.getElementById("connCouID");
		let connBank = document.getElementById("connBankID");
		let tip = document.getElementById("tip");
		connCou.value = couID;
		connBank.value = bankID;
		tip.innerText = bankName;
	}

</script>

<?php
// 引入尾部文件
require_once '../../../base/footer-iframe.php';

?>

