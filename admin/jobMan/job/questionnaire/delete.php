<?php


ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../../sql/connection.php';

$deleteID = $_POST['deleteID'];
$deleteJobID = $_POST['deleteJobID'];

// 查询语句
$query_sql = "select id, create_time, update_time, question_ID, job_ID, is_delete from `job-question` where id = '$deleteID'";
// 连接数据库，并查询
$result = mysqli_query($GLOBALS['conn'], $query_sql);


$now = date('Y-m-d H:i:s', time());
$deleteSql = "UPDATE `job-question` set is_delete = 1 where job_ID = '$deleteJobID' and question_ID = '$deleteID'";

if (mysqli_query($GLOBALS['conn'], $deleteSql)) {

	// 查询作业所包含的题目（SQL语句）
	$queryQuestionSql = "select question_type, question_opt_answer, question_completion_answer from `question_info`
              		where id IN (select question_ID from `job-question` where job_ID = '$deleteJobID' and `job-question`.is_delete = 0) and is_delete = 0";
	// 临时存储作业答案
	$jobAanswers = "本作业答案：";

	// 连接数据库，并查询
	$resultQue = mysqli_query($GLOBALS['conn'], $queryQuestionSql);
	// 题目数
	$queNum = mysqli_num_rows($resultQue);
	// 拼接答案
	while ($row = mysqli_fetch_array($resultQue)) {
		if ($row['question_type'] == 1) {
			$jobAanswers = $jobAanswers . $row['question_opt_answer'];
		} elseif ($row['question_type'] == 2) {
			$jobAanswers = $jobAanswers . $row['question_completion_answer'];
		} elseif ($row['question_type'] == 3) {
			$jobAanswers = $jobAanswers . "本题为简答题，由教师自行判断";
		}
		
		$jobAanswers = $jobAanswers . '；';
	}
	// 将答案存储到数据库中（SQL语句）
	$updateAnswerSql = "UPDATE  `job_info` SET job_answers = '$jobAanswers' ,question_total = '$queNum' where id = '$deleteJobID';";
	
	if (mysqli_query($GLOBALS['conn'], $updateAnswerSql)) {
			echo "
	    <script>
	        alert('删除成功！');
	        window.location.href=document.referrer;
	    </script>
	    ";
	}
	
} else {
	echo "
    <script> 
        alert('删除失败！');
        history.back();
    </script>
    ";
}

