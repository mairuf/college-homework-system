<?php
// 引入头部文件
require_once '../../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../../sql/connection.php';

$courseID = $_GET['courseID'];
$jobID = $_GET['jobID'];
//phpinfo();
?>

<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>题目列表</a>
		</li>
		<li class='nav-item'>
			<form class="form-inline" action="index.php" method="get" id="searchFrom">
				<input class="form-control mr-sm-2" type="search"
				       placeholder="搜索权限" aria-label="Search"
				       id="searchInfo" name="searchInfo"
				>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
			</form>
		</li>
		<li class='nav-item'>
			<div class="btn-group " style="padding: 5px">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
				        aria-haspopup="true" aria-expanded="false">
					未添加
				</button>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="index.php?courseID=<?= $courseID ?>&jobID=<?= $jobID ?>">已添加</a>
					<a class="dropdown-item" href="index0.php?courseID=<?= $courseID ?>&jobID=<?= $jobID ?>">未添加</a>
				</div>
			</div>
		</li>
	</ul>
	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>标题</th>
			<th scope='col'>题干</th>
			<th scope='col'>类型</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		$bankID = $_GET['bankID'];
		// echo "<script>console.log('$bankID')</script>";
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo'] ?: null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		if ($searchInfo == null) {
			// 统计管理员总数
			$countSql = "select count(*) as count from question_info where is_delete = 0 and
                              bank_ID IN (SELECT bank_ID FROM `course-bank` WHERE course_ID = '$courseID')
                                  AND id NOT IN (SELECT question_ID FROM `job-question` WHERE job_ID = '$jobID' and `job-question`.is_delete = 0);";
			$selectSql = "select * from question_info where is_delete = 0 and
                              bank_ID IN (SELECT bank_ID FROM `course-bank` WHERE course_ID = '$courseID')
                              AND id NOT IN (SELECT question_ID FROM `job-question` WHERE job_ID = '$jobID' and `job-question`.is_delete = 0) limit $index, 10;";
		} // 有搜索信息
		else {
			$countSql = "select count(*) as count from question_info where is_delete = 0 and and
                              bank_ID IN (SELECT bank_ID FROM `course-bank` WHERE course_ID = '$courseID')
                                              AND id NOT IN SELECT question_ID FROM `job-question` WHERE job_ID = '$jobID' and `job-question`.is_delete = 0) permission_info.name like '%$searchInfo%';";
			$selectSql = "select * from question_info where is_delete = 0 and question_info.question_title and
                                  bank_ID IN (SELECT bank_ID FROM `course-bank` WHERE course_ID = '$courseID')
                              AND id NOT IN (SELECT question_ID FROM `job-question` WHERE job_ID = '$jobID' and `job-question`.is_delete = 0) like '%$searchInfo%' limit $index, 10;";
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			// 判断是否是超管
			?>
			<tr>
				<td><?= $row['question_title'] ?></td>
				<td><?= $row['question_description'] ?></td>
				<td>
					<?php
					switch ($row['question_type']) {
						case 1:
							echo "选择题";
							break;
						case 2:
							echo "填空题";
							break;
						case 3:
							echo "简答题";
							break;
					}
					?>
				</td>
				<td>
					<button class='btn btn-outline-primary' data-toggle='modal' data-target='#add'
					        onclick='update("<?= $row['id'] ?>", "<?= $jobID ?>", "<?= $row['question_title'] ?>")'>添加
					</button>
				</td>
			</tr>

			<?php
		};

		?>
		</tbody>
	</table>
	<!-- 添加部件 -->
	<div class='modal fade bd-example-modal-lg' id='add' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>添加题目</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='add.php' method='post' target='adminFrame' name="addForm" id="addForm">
					<div class='modal-body'>
						<span class="h4">是否确定添加标题为
							<span id="qTitle" class="text-success h2">
							
							</span> 的题目？
						</span>
						<div class='form-group row hiddenElement'>
							<label for='questionID' class='col-3 col-form-label'>题目ID</label>
							<div class='col-sm-8'>
								<input type='text' id='questionID' name='questionID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required' value="<?= $courseID ?>"
								>
							</div>
						</div>
						<div class='form-group row hiddenElement'>
							<label for='jobID' class='col-3 col-form-label'>作业ID</label>
							<div class='col-sm-8'>
								<input type='text' id='jobID' name='jobID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
						<hr>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='addBtn' name='addBtn' class='btn btn-primary'>添加</button>
					</div>
				</form>
			</div>


		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="index.php?page=1&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="index.php?page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link"
			                         href="index.php?page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let search = document.getElementById("searchInfo");

	search.value = "<?= $searchInfo ?>";

	let update = function (questionID, jobID, title) {
		let qID = document.getElementById("questionID");
		let jID = document.getElementById("jobID");
		let qTitle = document.getElementById("qTitle");

		qID.value = questionID;
		jID.value = jobID;
		qTitle.innerText = title;

	};
	let deletePer = function (id, name) {
		let deleteInfo = document.getElementById("deleteInfo");
		let deleteID = document.getElementById("deleteID");

		console.log(id);
		console.log(deleteID);
		deleteInfo.innerText = name;
		deleteID.value = id;
	}


</script>

<?php
// 引入尾部文件
require_once '../../../base/footer-iframe.php';

?>

