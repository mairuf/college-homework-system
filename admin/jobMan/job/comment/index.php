<?php
// 引入头部文件
require_once '../../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../../sql/connection.php';

$jobID = $_GET['jobID'];
//phpinfo();
?>

<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>评论列表</a>
		</li>
	</ul>
	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>发表时间</th>
			<th scope='col'>发表人学号</th>
			<th scope='col'>发表人姓名</th>
			<th scope='col'>内容</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		$bankID = $_GET['bankID'];
		// echo "<script>console.log('$bankID')</script>";
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo']? : null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		if ($searchInfo == null) {
			// 统计管理员总数
			$countSql = "select count(*) as count from comment where is_delete = 0 and jobID = '$jobID';";
			$selectSql = "select id, create_time, update_time, is_delete, jobID, stu_ID, stu_name, content from comment where is_delete = 0 and jobID = '$jobID' limit $index, 10;";
		} // 有搜索信息
		else {
			$countSql = "select count(*) as count from comment where is_delete = 0 and jobID = '$jobID' and stu_name like '%$searchInfo%' limit $index, 10;";
			$selectSql = "select id, create_time, update_time, is_delete, jobID, stu_ID, stu_name, content from comment where is_delete = 0 and jobID = '$jobID' and stu_name like '%$searchInfo%' limit $index, 10;";
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			?>
			<tr>
				<td><?= $row['create_time'] ?></td>
				<td><?= $row['stu_ID'] ?></td>
				<td><?= $row['stu_name'] ?></td>
				<td><?= $row['content'] ?></td>
				<td>
					<button class='btn btn-outline-danger' data-toggle='modal' data-target='#delete'
					        onclick='deleteC(<?= $row['id'] ?>, "<?= $row['stu_ID'] ?>" , "<?= $row['stu_name'] ?>",  "<?= $row['content'] ?>")'>删除
					</button>
				</td>
			</tr>

			<?php
		};

		?>
		</tbody>
	</table>
	<!-- 删除部件 -->
	<div class='modal fade' id='delete' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>删除评论</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action="delete.php" id="deleteForm" name="deleteForm" method="post">
					<div class='modal-body'>
						<p>
							是否确认删除学号为：“
							<span id="deleteStuNum" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>”
							姓名为：”
							<span id="deleteStuName" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ 的学生发表的，内容为：
							<span id="deleteStuCont" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							的评论？
						</p>
						<div class='form-group row hiddenElement'>
							<label for='deleteID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id="deleteID" name="deleteID" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='deleteBtn' name='deleteBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="index.php?jobID=<?= $jobID ?>&page=1&searchInfo=<?= $searchInfo ?>" target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?jobID=<?= $jobID ?>&page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="index.php?jobID=<?= $jobID ?>&page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?jobID=<?= $jobID ?>&page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link" href="index.php?jobID=<?= $jobID ?>&page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let deleteC = function (id, stuNum , stuName, cont) {
		console.log(id);
		console.log(stuNum);
		console.log(stuName);
		console.log(cont);
		let deleteID = document.getElementById("deleteID");
		let deleteStuNum = document.getElementById("deleteStuNum");
		let deleteStuName = document.getElementById("deleteStuName");
		let deleteStuCont = document.getElementById("deleteStuCont");
		deleteID.value = id;
		deleteStuNum.innerText = stuNum;
		deleteStuName.innerText = stuName;
		deleteStuCont.innerText = cont;
	}


</script>

<?php
// 引入尾部文件
require_once '../../../base/footer-iframe.php';

?>

