<?php

// ini_set("display_errors", "On");
// error_reporting(E_ALL);

require_once '../../../base/header-iframe.php';

require_once '../../../../sql/connection.php';

$jobID = $_GET['jobID'];
$stuID = $_GET['stuID'];
$courseID = $_GET['courseID'];
$classID = $_GET['classID'];

// 查询学生提交答案：
// 查询语句
$querySql = "select answers,answer_file from `student-job` where is_delete = 0 and job_ID = '$jobID' and stu_ID = '$stuID';";
// 连接数据库，并查询
$resultConfig = mysqli_query($GLOBALS['conn'], $querySql);
// 获取学生答案
$row = mysqli_fetch_array($resultConfig);
$answers = substr($row['answers'], 36);
$answersFile = $row['answer_file'];

// 查询作业参考答案：
// 查询语句
$queryJobSql = "select job_answers,job_type from `job_info` where is_delete = 0 and id = '$jobID';";
// 连接数据库，并查询
$resultJob = mysqli_query($GLOBALS['conn'], $queryJobSql);
// 获取作业答案
$jobRow = mysqli_fetch_array($resultJob);
$jobAnswers = substr($jobRow['job_answers'], 18);
$jobType = $jobRow['job_type'];
?>
	<div class="container-fluid h-100">

		<!-- 导航部分信息 -->
		<ul class='nav justify-content-center'>
			<li class='nav-item'>
				<a class='nav-link active' href=''>刷新界面</a>
			</li>
			<li class='nav-item'>
				<a class='nav-link active' href='index.php?courseID=<?=$courseID?>&jobID=<?=$jobID?>'>返回班级</a>
			</li>
			<li class='nav-item'>
				<a class='nav-link active' href='list.php?courseID=<?=$courseID?>&jobID=<?=$jobID?>&classID=<?= $classID ?>'>返回学生</a>
			</li>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</ul>

		<?php
		if ($jobType == 1) {
			?>
			<!-- 以卡片的形式展示参考答案 -->
			<div class="card text-center">
				<!-- 题目标题 -->
				<div class="card-header" style="background: rgba(63,167,220,0.82)">
					<h5 class="card-title">本作业参考答案（按题目顺序）：</h5>
				</div>
				<!-- 题目描述 -->
				<div class="card-body" style="background: rgba(144,210,183,0.82)">
					<h5 class="card-title"><?= $jobAnswers ?></h5>
				</div>
			</div>
			<br><br>
			<!-- 以卡片的形式展示学生答案 -->
			<div class="card text-center">
				<!-- 题目标题 -->
				<div class="card-header" style="background: rgba(192,192,192,0.82)">
					<h5 class="card-title">学生上次提交的答案（按题目顺序）：</h5>
				</div>
				<!-- 题目描述 -->
				<div class="card-body" style="background: rgba(94,162,159,0.82)">
					<h5 class="card-title"><?= $answers ?></h5>
				</div>
			</div>
			<br><br>
		<?php } else { ?>
			<!-- 以卡片的形式展示学生答案 -->
			<div class="card text-center">
				<!-- 题目标题 -->
				<div class="card-header" style="background: rgba(255,255,255,0.82)">
					<h5 class="card-title">学生上次提交的文件（当前作业为自定义类型，请下载附件进行查看）：
						<a class='btn btn-outline-primary' href="../../../..<?= $answersFile ?>">下载附件</a>
					</h5>
				</div>
			</div>
			<br><br>

		<?php } ?>
		<!-- 以卡片的形式展示给分模块 -->
		<div class="card text-center">
			<!-- 题目标题 -->
			<div class="card-header" style="background: rgba(92,147,104,0.82)">
				<h5 class="card-title">给分</h5>
			</div>
			<!-- 题目描述 -->
			<div class="card-body" style="background: rgba(255,255,255,0.82)">
				<form name="correctingForm" action="correctingAction.php" method="post">

					<div class="d-inline hiddenElement">
						<label for="stuID">学生ID</label>
						<input type="text" id="stuID" name="stuID" value="<?= $stuID ?>">

					</div>
					<label for="score">分数</label>
					<input type="number" id="score" name="score">
					<button class="btn btn-group-lg badge-success" type="submit">提交</button>
					<div class="d-inline hiddenElement">
						<label for="jobID">作业ID</label>
						<input type="text" id="jobID" name="jobID" value="<?= $jobID ?>">
					</div>
				</form>
			</div>
		</div>
		<br><br>

	</div>
<?php require_once '../../../base/footer-iframe.php' ?>