<?php
// 引入头部文件
require_once '../../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../../sql/connection.php';

//phpinfo();

$jobID = $_GET['jobID'];
$courseID = $_GET['courseID'];
$classID = $_GET['classID'];
$countSql = "select count(*) as count from student_info where is_delete = 0
            	and id IN (SELECT stu_ID FROM `student-job` WHERE job_ID = '$jobID'
                              and `student-job`.stu_ID in (select id from student_info where `student-job`.is_delete = 0 and class_ID = '$classID'))
				;";
// 获取管理员总数
$submitNum = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
$classStuCountSql = "select count(*) as count from student_info where is_delete = 0 and class_ID = '$classID';";
// 获取管理员总数
$classStuCount = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $classStuCountSql));
?>

<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>学生作业列表（只展示已提交的学生）</a>
		</li>
		<li class='nav-item'>
			<a class='nav-link active' href='index.php?courseID=<?=$courseID?>&jobID=<?=$jobID?>'>返回班级列表</a>
		</li>
	</ul>
	<div class="row justify-content-center">
		<h5>
			当前班级总人数为：<span style="color: #00c3ff;font-size: 30px"><?= $classStuCount['count'] ?></span>&nbsp;&nbsp;人，
			当前作业已提交人数为：<span style="color: #0051ff;font-size: 30px"><?= $submitNum['count'] ?></span>&nbsp;&nbsp;人，
			还有：<span style="color: #ff0000;font-size: 30px"><?= $classStuCount['count'] - $submitNum['count'] ?></span>&nbsp;&nbsp;人未交
		</h5>
	</div>
	<!-- 列表 -->
	<table class='table table-striped text-center'>
		
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>学生名称</th>
			<th scope='col'>本次作业成绩</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo'] ?: null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		// 统计班级总数
		// $countSql = "select count(*) as count from student_info where is_delete = 0 and id IN (SELECT stu_ID FROM `student-job` WHERE job_ID = '$jobID');";
		$selectSql = "SELECT
						student_info.id,
						student_info.stu_name,
						`student-job`.score
					FROM
						student_info,
						`student-job`
						
						WHERE
						`student-job`.stu_ID = student_info.id
						AND class_ID = $classID
						AND job_ID = '$jobID'
						AND student_info.id IN (SELECT stu_ID FROM `student-job` WHERE job_ID = '$jobID')";
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			?>


			<tr>
				<td><?= $row['stu_name'] ?></td>
				<?php
				if ($row['score'] != null) {
					?>
					<td><h4 style='color: rgb(255,0,0)'><?= $row['score'] ?>&nbsp;&nbsp;分</h4></td>
					<td>

						<a class='btn btn-outline-primary'
						   href="correcting.php?courseID=<?=$courseID?>&jobID=<?= $jobID ?>&stuID=<?= $row['id'] ?>&classID=<?= $classID ?>">重新批改</a>
					</td>
					<?php
				} else { ?>

					<td><h5 style='color: rgba(0,163,168,0.87)'>未批改</h5></td>
					<td>

						<a class='btn btn-outline-primary'
						   href="correcting.php?courseID=<?=$courseID?>&jobID=<?= $jobID ?>&stuID=<?= $row['id'] ?>&classID=<?= $classID ?>">批改作业</a>
					</td>
				<?php }
				?>

			</tr>

			<?php
		};

		?>
		</tbody>
	</table>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="list.php?jobID=<?= $jobID ?>&classID=<?=$classID?>&page=1&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="list.php?jobID=<?= $jobID ?>&classID=<?=$classID?>&page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="list.php?jobID=<?= $jobID ?>&classID=<?=$classID?>&page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="list.php?jobID=<?= $jobID ?>&classID=<?=$classID?>&page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link"
			                         href="list.php?jobID=<?= $jobID ?>&classID=<?=$classID?>&page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let search = document.getElementById("searchInfo");

	search.value = "<?= $searchInfo ?>";

	let update = function (id, name) {
		let editID = document.getElementById("editID");
		let editName = document.getElementById("editName");
		console.log(editID);

		editID.value = id;
		editName.value = name;

	};
	let deletePer = function (id, name) {
		let deleteInfo = document.getElementById("deleteInfo");
		let deleteID = document.getElementById("deleteID");

		console.log(id);
		console.log(deleteID);
		deleteInfo.innerText = name;
		deleteID.value = id;
	}


</script>

<?php
// 引入尾部文件
require_once '../../../base/footer-iframe.php';

?>

