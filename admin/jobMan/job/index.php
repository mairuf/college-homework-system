<?php
// 引入头部文件
require_once '../../base/header-iframe.php';
// 引入数据库连接文件
require_once '../../../sql/connection.php';

//phpinfo();

$courseID = $_GET['courseID'];

?>

<!-- 主体部分 -->
<div class="container-fluid h-100">
	<!-- 导航部分信息 -->
	<ul class='nav justify-content-center'>
		<li class='nav-item'>
			<a class='nav-link active' href=''>作业列表</a>
		</li>
		<li class='nav-item'>
			<a class='nav-link' href='#' data-toggle='modal' data-target='#add'>添加作业</a>
		</li>
		<li class='nav-item'>
			<a class='nav-link' href='#' data-toggle='modal' data-target='#generate'>生成作业（问卷形式）</a>
		</li>
		<li>
			<form class="form-inline" action="index.php" method="get" id="searchFrom">
				<input class="form-control mr-sm-2" type="search"
				       placeholder="搜索作业" aria-label="Search"
				       id="searchInfo" name="searchInfo"
				>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
			</form>
		</li>
	</ul>
	<!-- 列表 -->
	<table class='table table-striped text-center'>
		<thead>
		<tr>
			<!--        <th scope='col'>id</th>-->
			<th scope='col'>创建时间</th>
			<th scope='col'>最后一次修改时间</th>
			<th scope='col'>作业名</th>
			<th scope='col'>作业类型</th>
			<th scope='col'>题目总数</th>
			<th scope='col'>总分</th>
			<th scope='col'>操作</th>
		</tr>
		</thead>
		<tbody>

		<?php
		// 搜索信息，如果没有传入搜索信息，则设为 null
		$searchInfo = $_GET['searchInfo'] ?: null;
		// 页码
		$pageNum = intval($_GET['page'] ?: 1);
		// 数据库索引 ——
		$index = ($pageNum - 1) * 10;

		// 无搜索信息
		if ($searchInfo == null) {
			// 统计管理员总数
			$countSql = "select count(*) as count from job_info where is_delete = 0;";
			$selectSql = "select id, create_time, update_time, course_ID, job_name, job_type, question_total, job_total_points, is_delete from job_info where is_delete = 0 limit $index, 10;";
		} // 有搜索信息
		else {
			$countSql = "select count(*) as count from job_info where is_delete = 0 and  job_info.job_name like '%$searchInfo%';";
			$selectSql = "select id, create_time, update_time, course_ID, job_name, job_type, question_total, job_total_points, is_delete from job_info where is_delete = 0 and
                                                                                                          job_name like '%$searchInfo%' limit $index, 10;";
		}
		// 连接数据库，并查询
		$resultConfig = mysqli_query($GLOBALS['conn'], $selectSql);
		// 获取管理员总数
		$count = mysqli_fetch_array(mysqli_query($GLOBALS['conn'], $countSql));
		// 计算总页数，使用 ceil 函数取整（只要有小数位就向上取整，即整体 +1 并去掉小数位）,然后将页数转为整数类型
		$page_sum = intval(ceil($count['count'] / 10));

		// 循环输出所有查询结果
		while ($row = mysqli_fetch_array($resultConfig)) {
			?>


			<tr>
				<td><?= $row['create_time'] ?></td>
				<td><?= $row['update_time'] ?></td>
				<td><?= $row['job_name'] ?></td>
				<td><?= $row['job_type'] == 1 ? "问卷形式" : "自定义形式" ?></td>
				<td><?= $row['question_total'] ?></td>
				<td><?= $row['job_total_points'] ?></td>
				<td>
					<?php
					if ($row['job_type'] == 1) {
						?>
						<a href="questionnaire/index.php?courseID=<?= $courseID?>&jobID=<?= $row['id'] ?>" class="btn btn-outline-primary">添加题目</a>
						<?php
					} else {
						?>
						<a class='btn btn-outline-primary' href='#' data-toggle='modal' data-target='#upload' onclick='uploadFile(<?= $row['id'] ?>,"<?= $row['job_name'] ?>")'>上传附件</a>
						<?php
					}
					?>
					<a class='btn btn-outline-primary' href="studentJob/index.php?courseID=<?= $courseID?>&jobID=<?= $row['id']?>">查看作业提交情况
					</a>
					<a class='btn btn-outline-primary' href="comment/index.php?jobID=<?= $row['id']?>">查看作业评论
					</a>
					<button class='btn btn-outline-primary' data-toggle='modal' data-target='#edit'
					        onclick='update(<?= $row['id'] ?>,"<?= $row['nikeName'] ?>", "<?= $row['name'] ?>")'>编辑
					</button>
					<button class='btn btn-outline-danger' data-toggle='modal' data-target='#delete'
					        onclick='deletePer(<?= $row['id'] ?>, "<?= $row['name'] ?>")'>删除
					</button>
				</td>
			</tr>

			<?php
		};

		?>
		</tbody>
	</table>

	<!-- 添加部件 -->
	<div class='modal fade bd-example-modal-lg' id='add' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>添加作业</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='add.php' method='post' target='adminFrame' name="addForm" id="addForm">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='addCourseID' class='col-3 col-form-label'>课程ID</label>
							<div class='col-sm-8'>
								<input type='text' id='addCourseID' name='addCourseID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required' value="<?= $courseID ?>"
								>
							</div>
						</div>
						<div class='form-group row'>
							<label for='addName' class='col-3 col-form-label'>作业名称</label>
							<div class='col-sm-8'>
								<input type='text' id='addName' name='addName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
						<div class='form-group row'>
							<label for='addJobTotalPoints' class='col-3 col-form-label'>总分数</label>
							<div class='col-sm-8'>
								<input type='text' id='addJobTotalPoints' name='addJobTotalPoints'
								       class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>
						<div class='form-group row '>
							<label for='addJType' class='col-8 col-form-label'>作业类型</label>
							<div class='col-sm-12'>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="type1" name="addJType" class="custom-control-input"
									       onclick="activeInput('选择题')" value="1">
									<label class="custom-control-label" for="type1">问卷</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="type2" name="addJType" class="custom-control-input"
									       onclick="activeInput('填空题')" value="2">
									<label class="custom-control-label" for="type2">自定义</label>
								</div>
							</div>
						</div>
						<br><br>

						<hr>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='addBtn' name='addBtn' class='btn btn-primary'>添加</button>
					</div>
				</form>
			</div>


		</div>
	</div>
	<!-- 生成部件 -->
	<div class='modal fade bd-example-modal-lg' id='generate' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>生成作业</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='generateAction.php' method='post' target='adminFrame' name="addForm" id="addForm">
					<div class="modal-header">
						<span style="font-size: 16px; color: red">
							注意：如果设置的某一类型的题目数量<b>&nbsp;&nbsp;多于&nbsp;&nbsp;</b>题库中对应类型的题目数，则会将<b>&nbsp;&nbsp;所有&nbsp;&nbsp;</b>该类型的题目都添加至该次作业
						</span>
					</div>
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='addCourseID' class='col-3 col-form-label'>课程ID</label>
							<div class='col-sm-8'>
								<input type='text' id='addCourseID' name='addCourseID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required' value="<?= $courseID ?>"
								>
							</div>
						</div>
						<div class='form-group row'>
							<label for='addName' class='col-3 col-form-label'>作业名称</label>
							<div class='col-sm-8'>
								<input type='text' id='addName' name='addName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br>
						<div class='form-group row'>
							<label for='addJobTotalPoints' class='col-3 col-form-label'>总分数</label>
							<div class='col-sm-8'>
								<input type='text' id='addJobTotalPoints' name='addJobTotalPoints'
								       class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br>
						<div class='form-group row'>
							<label for='optNum' class='col-3 col-form-label'>选择题数量</label>
							<div class='col-sm-8'>
								<input type='number' id='optNum' name='optNum'
								       class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								       value="0"
								>
							</div>
						</div>
						<br>
						<div class='form-group row'>
							<label for='completionNum' class='col-3 col-form-label'>填空题数量</label>
							<div class='col-sm-8'>
								<input type='number' id='completionNum' name='completionNum'
								       class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								       value="0"
								>
							</div>
						</div>
						<br>
						<div class='form-group row'>
							<label for='shortNum' class='col-3 col-form-label'>简答题数量</label>
							<div class='col-sm-8'>
								<input type='number' id='shortNum' name='shortNum'
								       class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								       value="0"
								>
							</div>
						</div>
						<br>
						<hr>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='addBtn' name='addBtn' class='btn btn-primary'>生成</button>
					</div>
				</form>
			</div>


		</div>
	</div>
	<!-- 文件上传部件 -->
	<div class='modal fade bd-example-modal-lg' id='upload' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle modal-lg' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>上传模板</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='uploadAction.php' method='post' target='adminFrame' name="addForm" id="addForm" enctype="multipart/form-data">
					<div class='modal-body'>
						<div class='form-group row hiddenElement'>
							<label for='upCourseID' class='col-3 col-form-label'>课程ID</label>
							<div class='col-sm-8'>
								<input type='text' id='upCourseID' name='upCourseID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required' value="<?= $courseID ?>"
								>
							</div>
						</div>
						<div class='form-group row hiddenElement'>
							<label for='upJobID' class='col-3 col-form-label'>作业ID</label>
							<div class='col-sm-8'>
								<input type='text' id='upJobID' name='upJobID' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<div class='form-group row hiddenElement'>
							<label for='upJobName' class='col-3 col-form-label'>作业名称</label>
							<div class='col-sm-8'>
								<input type='text' id='upJobName' name='upJobName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						
						<div class='form-group row'>
							<label for='templateFile' class='col-3 col-form-label'>文件</label>
							<div class='col-sm-8'>
								<input type='file' id='templateFile' name='templateFile'
								       class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline' required='required'
								>
							</div>
						</div>
						<br><br>

						<hr>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='upBtn' name='upBtn' class='btn btn-primary'>上传</button>
					</div>
				</form>
			</div>


		</div>
	</div>
	<!-- 编辑部件 -->
	<div class='modal fade ' id='edit' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content '>
				<div class='modal-header'>
					<h5 class='modal-title ' id='exampleModalCenterTitle'>编辑作业信息</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action='edit.php' method='post' target='adminFrame' name="editForm" id="editForm">
					<div class='modal-body'>
						<div class='form-group row'>
							<label for='editID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id='editID' name='editID' class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
						<br><br>

						<div class='form-group row'>
							<label for='editName' class='col-3 col-form-label'>作业名称：</label>
							<div class='col-sm-8'>
								<input type='text' id='editName' name='editName' class='form-control mx-sm-3'
								       aria-describedby='nameHelpInline'
								       required='required'
								>
							</div>
						</div>
						<br><br>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='editBtn' name='editBtn' class='btn btn-primary'>更改</button>
					</div>
				</form>
			</div>

		</div>
	</div>
	<!-- 删除部件 -->
	<div class='modal fade' id='delete' tabindex='-1' role='dialog'
	     aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
		<div class='modal-dialog modal-dialog-centered' role='document'>
			<div class='modal-content'>
				<div class='modal-header'>
					<h5 class='modal-title text-center' id='exampleModalCenterTitle'>删除作业</h5>
					<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
					</button>
				</div>
				<form action="delete.php" id="deleteForm" name="deleteForm" method="post">
					<div class='modal-body'>
						<p>
							是否确认删除名为：”
							<span id="deleteInfo" class="text-center text-success font-weight-bold"
							      style="font-size: 30px"></span>
							“ 的作业？
						</p>
						<div class='form-group row hiddenElement'>
							<label for='deleteID' class='col-3 col-form-label'>I&nbsp;&nbsp;&nbsp;&nbsp;D：</label>
							<div class='col-sm-8'>
								<input type='text' id="deleteID" name="deleteID" class='form-control mx-sm-3'
								       readOnly='readonly'>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>取消</button>
						<button type='submit' id='deleteBtn' name='deleteBtn' class='btn btn-primary'>确定</button>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
<!-- 分页部分 -->
<div class="container-fluid">
	<?php
	// 下一页
	$next_page = 0;
	//上一页
	$pre_page = 0;

	if ($pageNum > 1) {
		$pre_page = $pageNum - 1;
	} else {
		$pre_page = $pageNum;
	}

	if ($pageNum < $page_sum) {
		$next_page = $pageNum + 1;
	} else {
		$next_page = $pageNum;
	}
	?>
	<!-- 分页按钮 -->
	<nav aria-label="Page navigation example">
		<ul class="pagination justify-content-center">
			<!-- 第一页按钮 -->
			<li class="page-item"><a class="page-link" href="index.php?page=1&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">首页</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 上一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?page=<?= $pre_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>

			<?php
			//循环开始
			for ($i = 1; $i <= $page_sum; $i++) {
				// 是否激活
				$is_active = "";
				if ($i == $pageNum) {
					$is_active = "active";
				}
				?>

				<li class="page-item <?= $is_active ?>">
					<a class="page-link "
					   href="index.php?page=<?= $i ?>&searchInfo=<?= $searchInfo ?>"><?= $i ?>
					</a>
				</li>
				<?php
			} // 循环结束


			?>
			<!-- 下一页按钮 -->
			<li class="page-item">
				<a class="page-link" href="index.php?page=<?= $next_page ?>&searchInfo=<?= $searchInfo ?>"
				   aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
			<!-- 第最后一页按钮 -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="page-item"><a class="page-link"
			                         href="index.php?page=<?= $page_sum ?>&searchInfo=<?= $searchInfo ?>"
			                         target="adminFrame">尾页</a></li>
		</ul>
	</nav>
	<!-- 分页信息 -->
	<div class="col-12 text-center">
		共&nbsp;&nbsp;<span class="pageInfoText"><?= $count['count'] ?></span>&nbsp;&nbsp;条数据，每页显示&nbsp;<span
				class="pageInfoText">10</span>&nbsp;条数据，共&nbsp;&nbsp;<span class="pageInfoText"><?= $page_sum ?></span>&nbsp;&nbsp;页
	</div>
</div>

<!-- JavaScript代码 -->
<script>

	let search = document.getElementById("searchInfo");

	search.value = "<?= $searchInfo ?>";

	let update = function (id, name) {
		let editID = document.getElementById("editID");
		let editName = document.getElementById("editName");
		console.log(editID);

		editID.value = id;
		editName.value = name;

	};
	let deletePer = function (id, name) {
		let deleteInfo = document.getElementById("deleteInfo");
		let deleteID = document.getElementById("deleteID");

		console.log(id);
		console.log(deleteID);
		deleteInfo.innerText = name;
		deleteID.value = id;
	}
	
	let uploadFile = function (id,name){
		let jobID = document.getElementById("upJobID");
		let jobName = document.getElementById("upJobName");
		jobID.value = id;
		jobName.value = name;
	}


</script>

<?php
// 引入尾部文件
require_once '../../base/footer-iframe.php';

?>

