<?php
ini_set("display_errors", "On");
error_reporting(E_ALL);

// 引入数据库连接文件
require_once '../../../sql/connection.php';

// 课程ID
$addCourseID = $_POST['addCourseID'];
// 作业名称
$addName = $_POST['addName'];
// 总分数
$addJobTotalPoints = $_POST['addJobTotalPoints'];
// 选择题数量
$optNum = $_POST['optNum'];
// 填空题数量
$completionNum = $_POST['completionNum'];
// 简答题数量
$shortNum = $_POST['shortNum'];

// 获取当前时间
$now = date('Y-m-d H:i:s', time());


// 添加作业
// SQL
$addJobSql = "INSERT INTO job_info(create_time, update_time, course_ID, job_name, job_type, question_total, job_total_points,  is_delete)
							VALUES ('$now',		'$now',	'$addCourseID','$addName', 1 ,		0 ,				'$addJobTotalPoints',	0)";

// 添加作业
// 添加成功
if (mysqli_query($GLOBALS['conn'], $addJobSql)) {
	// 1、获取刚添加的作业 id
	// SQL 语句
	$queryJobIdSql = "SELECT id FROM job_info WHERE course_ID = '$addCourseID' AND job_name = '$addName' AND is_delete = 0;";
	// 查询
	$resultOpt = mysqli_query($GLOBALS['conn'], $queryJobIdSql);
	// 获取结果
	$jobInfo = mysqli_fetch_array($resultOpt);
	$jobID = $jobInfo['id'];
	
	// 2、关联题目
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------
	// 获取选择题
	// SQL 语句
	$getOptSQL = "SELECT id FROM question_info WHERE
                bank_ID IN ( SELECT bank_ID FROM `course-bank` WHERE course_ID = '$addCourseID' AND is_delete = 0)
              	AND question_type = 1 AND is_delete = 0
                ORDER BY RAND() LIMIT " . "$optNum;";
	// 选择题结果集
	$resultOpt = mysqli_query($GLOBALS['conn'], $getOptSQL);
	// 关联选择题
	while ($row = mysqli_fetch_array($resultOpt)) {
		$id = $row['id'];
		// 查询语句
		$querySql = "INSERT INTO `job-question`(create_time, update_time,  question_ID, job_ID, is_delete)VALUES ('$now','$now','$id','$jobID',0)";
		// 连接数据库，并查询
		$result = mysqli_query($GLOBALS['conn'], $querySql);
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------
	// 获取填空题
	// SQL 语句
	$getComSQL = "SELECT id FROM question_info WHERE
                bank_ID IN ( SELECT bank_ID FROM `course-bank` WHERE course_ID = '$addCourseID' AND is_delete = 0)
              	AND question_type = 2 AND is_delete = 0
                ORDER BY RAND() LIMIT " . "$completionNum;";
	// 填空题结果集
	$resultCom = mysqli_query($GLOBALS['conn'], $getComSQL);
	// 关联填空题
	while ($row = mysqli_fetch_array($resultCom)) {
		$id = $row['id'];
		// 查询语句
		$querySql = "INSERT INTO `job-question`(create_time, update_time,  question_ID, job_ID, is_delete)VALUES ('$now','$now','$id','$jobID',0)";
		// 连接数据库，并查询
		$result = mysqli_query($GLOBALS['conn'], $querySql);
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------
	// 获取简答题
	// SQL 语句
	$getShortSQL = "SELECT id FROM question_info WHERE
                bank_ID IN ( SELECT bank_ID FROM `course-bank` WHERE course_ID = '$addCourseID' AND is_delete = 0)
              	AND question_type = 3 AND is_delete = 0
                ORDER BY RAND() LIMIT " . "$shortNum;";
	// 简答题结果集
	$resultShort = mysqli_query($GLOBALS['conn'], $getShortSQL);
	// 关联简答题
	while ($row = mysqli_fetch_array($resultShort)) {
		$id = $row['id'];
		// 查询语句
		$querySql = "INSERT INTO `job-question`(create_time, update_time,  question_ID, job_ID, is_delete)VALUES ('$now','$now','$id','$jobID',0)";
		// 连接数据库，并查询
		$result = mysqli_query($GLOBALS['conn'], $querySql);
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------
	// 查询作业所包含的题目（SQL语句）
	$queryQuestionSql = "select question_type, question_opt_answer, question_completion_answer from `question_info`
              		where id IN (select question_ID from `job-question` where job_ID = '$jobID' and `job-question`.is_delete = 0) and is_delete = 0";
	// 临时存储作业答案
	$jobAanswers = "本作业答案：";
	// 连接数据库，并查询
	$resultQue = mysqli_query($GLOBALS['conn'], $queryQuestionSql);
	// 题目数
	$queNum = mysqli_num_rows($resultQue);
	// 拼接答案
	while ($row = mysqli_fetch_array($resultQue)) {
		if ($row['question_type'] == 1) {
			$jobAanswers = $jobAanswers . $row['question_opt_answer'];
		} elseif ($row['question_type'] == 2) {
			$jobAanswers = $jobAanswers . $row['question_completion_answer'];
		} elseif ($row['question_type'] == 3) {
			$jobAanswers = $jobAanswers . "本题为简答题，由教师自行判断";
		}
		$jobAanswers = $jobAanswers . '；';
	}
	// 将答案存储到数据库中（SQL语句）
	$updateAnswerSql = "UPDATE  `job_info` SET job_answers = '$jobAanswers'  ,question_total = '$queNum' where id = '$jobID';";
	if (mysqli_query($GLOBALS['conn'], $updateAnswerSql)) {
		echo "
            <script>
                alert('生成成功！');
                window.location.href=document.referrer;
            </script>
        ";
	} else {
		echo "
            <script>
                alert('生成失败！');
                history.back();
            </script>
        ";
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------
} // 添加失败
else {
	echo "
            <script>
                alert('生成失败，作业未添加！');
                history.back();
            </script>
        ";
}


